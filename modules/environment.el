;;; environment.el --- Helper functions and variables -*- lexical-binding: t -*-
;;; Commentary:
;;
;;; Code:

;; got this from `https://emacs.stackexchange.com/questions/18205/how-can-i-distinguish-between-linux-distributions-in-emacs'
(defun +parse-os-release-file(regexp)
  "Parse `/etc/os-release' file for REGEXP."
  (let ((maybe-get-dis-str (shell-command-to-string "cat /etc/*release")))
    (with-temp-buffer
      (insert maybe-get-dis-str)
      (beginning-of-buffer)
      (condition-case nil
          (progn
            (search-forward-regexp regexp)
            (downcase (buffer-substring (match-beginning 1) (match-end 1))))
        (search-failed nil)))))

(defun +guess-linux-based-distribution()
  "Guess linux distribution family."
  (+parse-os-release-file "^ID_LIKE=\"?\\([a-zA-Z ]*\\)\"?$"))

(defun +guess-linux-distribution()
  "Guess linux distribution."
  (+parse-os-release-file "^ID=\"?\\(\\w*\\)\"?$"))

(defun +config/load-theme (theme)
  "Load THEME after new frame.
Also for the current frame"
  (progn
    (load-theme theme t)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (select-frame frame)
                (when (display-graphic-p frame)
                  (load-theme theme t))))))

(defvar +diminished-list '()
  "Modes to diminish.")

(defvar +emacs-data-dir
  (if (getenv "XDG_DATA_HOME")
      (expand-file-name "emacs" (getenv "XDG_DATA_HOME"))
    (expand-file-name ".local/share/emacs" (getenv "HOME")))
  "Location for Emacs data files.")

(defvar +distro (+guess-linux-distribution)
  "guess the running Linux/Unix distribution.")

(defvar +minimal-ui nil
  "Set minimal UI.")

(defvar +completion-uses-vertico nil
  "Use `vertico' completion frameworks")

(defvar +flycheck-enabled nil
  "Use `flycheck'.")

(defvar +vterm-enabled nil
  "Use `vterm'.")

(defvar +perspective-enabled nil
  "Use `perspective'")

(defvar +rainbow-enabled nil
  "Use various rainbow packages")

(defvar +hl-todo-enabled nil
  "Use `hl-todo'.")

(defvar +diff-hl-enabled nil
  "Use `diff-hl'.")

(defvar +treemacs-enabled nil
  "Use `treemacs'.")

(defvar +multiple-cursors-enabled nil
  "Use `multiple-cursors'.")

(defvar +yasnippet-enabled nil
  "Use `yasnippet'.")

(defvar +yaml-enabled nil
  "Use yaml packages.")

(defvar +toml-mode-enabled nil
  "Use `toml-mode'.")

(defvar +guix-enabled nil
  "Use `guix'.")

(defvar +geiser-enabled nil
  "Use `geiser'.")

(defvar +nix-mode-enabled nil
  "Use `nix'.")

(defvar +nginx-mode-enabled nil
  "Use `nginx-mode'.")

(defvar +markdown-enabled nil
  "Use markdown packages.")

(defvar +visual-regexp-enabled nil
  "Use `visual-regexp'.")

(defvar +undo-fu-enabled nil
  "Use undo-fu packages.")

(defvar +block-nav-enabled nil
  "Use `block-nav'.")

(defvar +vertico-posframe-enabled nil
  "Use `vertico-posframe'.")

(defvar +rust-mode-enabled nil
  "Use `rust-mode'.")

(defvar +ansible-enabled nil
  "Use ansible packages.")

(defvar +jinja2-mode-enabled nil
  "Use `jinja2-mode'.")

(defvar +hydra-enabled nil
  "Use `hydra'.")

(defvar +general-enabled nil
  "Use `general'.")

(defvar +evil-enabled nil
  "Use `evil'.")

(defvar +aggressive-indent-enabled nil
  "Use `aggressive-indent'.")

(defvar +docker-enabled nil
  "Use docker packages.")

(defvar +org-ref-enabled nil
  "Use `org-bibtex'.")

(defvar +org-roam-enabled nil
  "Use roam packages.")

(defvar +elfeed-enabled nil
  "Use `elfeed' packages.")

(defvar +org-journal-enabled nil
  "Use `org-journal'.")

(defvar +projectile-enabled nil
  "Use `projectile'.")

(defvar +lsp-enabled nil
  "Use lsp packages.")

(defvar +git-modes-enabled nil
  "Use `git-modes'.")

(defvar +sly-enabled nil
  "Use `sly'.")

(defvar +gist-enabled nil
  "Use `gist'.")

(defvar +tab-bar-enabled nil
  "Enable `tab-bar-mode'.")

(defvar +savehist-enabled nil
  "Enable `savehist-mode'.")

(defvar +saveplace-enabled nil
  "Enable `save-place-mode'.")

(defvar +emojify-enabled nil
  "Use `emojify'.")

(defvar +selectric-enabled nil
  "Use `selectric'.")

(defvar +symbol-overlay-enabled nil
  "Use `symbol-overlay'.")

(defvar +tempel-enabled nil
  "Use `symbol-overlay'.")

(defvar +notmuch-enabled nil
  "Use `notmuch'.")

(customize-set-variable 'no-littering-etc-directory (expand-file-name "etc/" +emacs-data-dir))
(customize-set-variable 'no-littering-var-directory (expand-file-name "var/" +emacs-data-dir))
(customize-set-variable 'straight-base-dir +emacs-data-dir)
(make-directory package-user-dir t)

(when (string= "guix" +distro)
  ;; adds the `site-lisp' directory from the current guix-profile
  ;; this is usually unnecessary, but just in case.
  (defun +guix-update-load-path ()
    (progn
      (when (getenv "GUIX_PROFILE")
        (let ((default-directory (expand-file-name "share/emacs/site-lisp/" (getenv "GUIX_PROFILE"))))
          (normal-top-level-add-subdirs-to-load-path)))
      (when (not (directory-empty-p
                  (expand-file-name ".guix-profile/share/emacs/site-lisp/" (getenv "HOME"))))
        (let ((default-directory (expand-file-name ".guix-profile/share/emacs/site-lisp/" (getenv "HOME"))))
          (normal-top-level-add-subdirs-to-load-path)))
      (when (not (directory-empty-p
                  (expand-file-name ".guix-home/profile/share/emacs/site-lisp" (getenv "HOME"))))
        (let ((default-directory (expand-file-name ".guix-home/profile/share/emacs/site-lisp" (getenv "HOME"))))
          (normal-top-level-add-subdirs-to-load-path)))))
  (+guix-update-load-path))

(defmacro +install-package (package &optional name)
  "Load PACKAGE or NAME. Falls back to `straight.el' or `package' when no other methods found."
  `(if (locate-library (if ,name (symbol-name ,name)
                         (symbol-name ,package)))
       (if ,name (require ,name) (require ,package))
     (cond
      ((and (string= "guix" +distro)
            (not
             (string=
              ""
              (shell-command-to-string
               (concat "guix search emacs-" (symbol-name ,package) "$")))))
       (shell-command (concat "guix install emacs-" (symbol-name ,package)) nil nil)
       (+guix-update-load-path))
      ((featurep 'straight)
       (if ,name
           (straight-use-package ,name)
         (straight-use-package ,package)))
      (t
       (progn
         (if ,name
             (require ,name))
         (require ,package))
       (setq package-user-dir (expand-file-name "var/elpa/" +emacs-data-dir))
       (customize-set-variable 'package-archives
                               '(("gnu" . "https://elpa.gnu.org/packages/")
                                 ("nongnu" . "https://elpa.nongnu.org/nongnu/")
                                 ("melpa" . "https://melpa.org/packages/")))
       (unless
           (featurep 'package-archive-contents)
         (package-refresh-contents))
       (if ,name
           (package-install ,name)
         (package-install ,package))))))

(defun +default/yank-buffer-path (&optional root)
  "Copy the current buffer's path to the kill ring."
  (interactive)
  (if-let (filename (or (buffer-file-name (buffer-base-buffer))
                        (bound-and-true-p list-buffers-directory)))
      (message "Copied path to clipboard: %s"
               (kill-new (abbreviate-file-name
                          (if root
                              (file-relative-name filename root)
                            filename))))
    (error "Couldn't find filename in current buffer")))

(defun +default/insert-file-path (arg)
  "Insert the file name (absolute path if prefix ARG).
If `buffer-file-name' isn't set, uses `default-directory'."
  (interactive "P")
  (let ((path (or buffer-file-name default-directory)))
    (insert
     (if arg
         (abbreviate-file-name path)
       (file-name-nondirectory path)))))

(defun +reload-config ()
  "Reload `Emacs' configuration."
  (interactive)
  (load user-init-file))

;; separate custom configuration from `init.el'.
(setq-default custom-file (expand-file-name "custom.el" +emacs-data-dir))
(when (file-exists-p custom-file)
  (load custom-file))

;; user-configuration files
(let ((user-config (expand-file-name "config-user.el" user-emacs-directory)))
  (when (file-exists-p user-config)
    (load-file user-config)))

(provide 'environment)
;;; environment.el ends here

;;; config-keybindings.el --- keybindings variables and helper functions -*- lexical-binding: t -*-
;;; Commentary:
;;
;;; Code:

;; disable annoying keys
(global-set-key (kbd "<menu>") nil)

(when +general-enabled
  (defconst leader-key "SPC")
  (defconst non-normal-leader-key "<menu>")
  (defconst local-leader-key "SPC m")
  (defconst non-normal-local-leader-key "C-<menu>")
  (general-create-definer leader-key-def
    :prefix leader-key
    :non-normal-prefix non-normal-leader-key)
  (general-create-definer local-leader-key-def
    :prefix local-leader-key
    :non-normal-prefix non-normal-local-leader-key)

  ;; global keybindings
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "SPC" 'projectile-find-file
    "." 'find-file
    "," 'switch-to-buffer
    ":" 'execute-extended-command
    ";" 'eval-expression
    "a" '(:ignore t :which-key "Action Prefix")
    "A" 'org-agenda
    "b" '(:ignore t :which-key "Buffers Prefix")
    "c" '(:ignore t :which-key "Flycheck Prefix")
    "f" '(:ignore t :which-key "File/Frame Prefix")
    "g" '(:ignore t :which-key "Git Prefix")
    "i" '(:ignore t :which-key "Insert Prefix")
    "m" '(:ignore t :which-key "Minor Mode Prefix")
    "o" '(:ignore t :which-key "Org Prefix")
    "q" '(:ignore t :which-key "Exit/Restart/Reload Prefix")
    "r" '(:ignore t :which-key "Register/Bookmark Prefix")
    "s" '(:ignore t :which-key "Search Prefix")
    "t" '(:ignore t :which-key "Terminal/Tabs/Tree Prefix")
    "w" '(:ignore t :which-key "Window Prefix")
    "X" 'org-capture)

  ;; theme
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "at" '(:ignore t :which-key "Theme")
    "ats" 'toggle-scroll-bar
    "atf" 'toggle-frame-tab-bar
    "atF" 'toggle-frame-fullscreen
    "atm" 'toggle-menu-bar-mode-from-frame
    "att" 'toggle-tool-bar-mode-from-frame)

  (when (featurep 'consult)
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "atc" 'consult-theme))

  (when (featurep 'theme-magic)
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "atw" 'theme-magic-from-emacs))

  ;; global keybinding active region
  (leader-key-def
    :states 'visual
    :keymaps 'override
    :predicate '(region-active-p)
    "\\" 'indent-region)

  ;; actions
  (when +selectric-enabled
    (leader-key-def
      :states '(normal visual)
      "as" '+toggle-selectric))

  ;; calendar
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "ac" 'calendar)

  ;; buffers
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "bb" 'switch-to-buffer
    "bR" 'rename-buffer
    "bd" 'kill-current-buffer
    "bi" 'ibuffer
    "bk" 'kill-buffer
    "bn" 'next-buffer
    "bo" 'evil-buffer
    "br" 'revert-buffer
    "bs" 'save-buffer
    "bS" 'save-some-buffers
    "bp" 'previous-buffer
    "b[" 'previous-buffer
    "b]" 'next-buffer)

  ;; eshell
  (leader-key-def
    :states '(normal visual)
    "te" 'eshell)

  ;; file / frame
  (leader-key-def
    :states '(normal visual)
    "fF" '(:ignore t :which-key "Frame Prefix")
    "fFc" 'clone-frame
    "fFf" 'find-file-other-frame
    "fFo" 'other-frame
    "fFd" 'delete-frame
    "fFD" 'delete-other-frames
    "f." 'find-file-at-point
    "ff" 'find-file
    "fy" '+default/yank-buffer-path)

  (when +completion-uses-vertico
    (leader-key-def
      :states '(normal visual)
      "fd" 'consult-dir
      "fj" 'consult-dir-jump-file
      "fl" 'consult-locate
      "fr" 'consult-recent-file))

  ;; git
  (when (featurep 'magit)
    (leader-key-def
      :states '(normal visual)
      "gb" 'magit-blame
      "gg" 'magit-status
      "gl" 'magit-log))

  (when +diff-hl-enabled
    (general-define-key
     :keymaps '(normal visual)
     :prefix leader-key
     :non-normal-prefix local-leader-key
     :repeat t
     :jump t
     "gj" 'diff-hl-next-hunk
     "gk" 'diff-hl-previous-hunk))

  ;; help
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "h" (general-simulate-key "C-h"
          :name +general-simulates-C-h
          :docstring "Simulates C-h in normal state with SPC h."
          :which-key "Help Prefix"))

  ;; insert
  (leader-key-def
    :states '(normal visual)
    "if" '(+default/insert-file-path :which-key "Insert file name")
    "iu" '(insert-char :which-key "Insert Unicode character"))

  (when +completion-uses-vertico
    (leader-key-def
      :states '(normal visual)
      "iy" '(consult-yank-pop :which-key "Insert from kill-ring")))

  (when +emojify-enabled
    (leader-key-def
      :states '(normal visual)
      "ie" 'emojify-insert-emoji))

  (when +yasnippet-enabled
    (leader-key-def
      :states '(normal visual)
      "is" '(yas-insert-snippet :which-key "Insert snippet")))

  (leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "il" 'org-insert-link
    "i C-M-l" 'org-insert-all-links)

  ;; perspective
  (when +perspective-enabled
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "TAB" (general-simulate-key "C-c TAB"
              :name +perspective-prefix
              :which-key "Perspective prefix")))
  ;; org
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "mls" 'org-store-link
    "mlL" 'org-insert-link-global)

  ;; org-journal
  (when +org-journal-enabled
    ;; global
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "oj" '(:ignore t :which-key "Org Journal Prefix")
      "ojs" '(:ignore t :which-key "Org Journal Search Prefix")
      "ojsm" 'org-journal-search-calendar-month
      "ojsy" 'org-journal-search-calendar-year
      "ojsw" 'org-journal-search-calendar-week
      "ojss" 'org-journal-search
      "ojsf" 'org-journal-search-future
      "ojsF" 'org-journal-search-forever
      "ojj" 'org-journal-new-entry
      "ojd" 'org-journal-new-date-entry
      "ojS" 'org-journal-new-scheduled-entry
      "ojr" 'org-journal-re-encrypt-journals
      "ojv" 'org-journal-schedule-view
      "ojo" '(:ignore t :which-key "Org Journal Open Prefix")
      "ojoo" 'org-journal-open-current-journal-file
      "ojon" 'org-journal-open-next-entry
      "ojop" 'org-journal-open-previous-entry)

    ;; org-journal-mode-map
    (local-leader-key-def
      :states '(normal visual)
      :keymaps 'org-journal-mode-map
      "oj C-n" 'org-journal-next-entry
      "oj C-p" 'org-journal-previous-entry)

    (general-define-key
     :keymaps 'calendar-mode-map
     "C-c j" '(:ignore t :which-key "Org Journal Prefix")
     "C-c jd" 'org-journal-display-entry
     "C-c jm" 'org-journal-mark-entries
     "C-c jr" 'org-journal-read-entry
     "C-c jR" 'org-journal-read-or-display-entry))

  ;; org-roam
  (when +org-roam-enabled
    ;; global
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "or" '(:ignore t :which-key "Org Roam Prefix")
      "orB" 'org-roam-buffer
      "orb" 'org-roam-buffer-toggle
      "ord" '(:ignore t :which-key "Org Roam Dailies Prefix")
      "ordf" 'org-roam-dailies-find-date
      "ordg" 'org-roam-dailies-goto-date
      "ordt" 'org-roam-dailies-find-today
      "ordT" 'org-roam-dailies-goto-today
      "ordx" 'org-roam-dailies-capture-date
      "ordX" 'org-roam-dailies-capture-today
      "ordn" 'org-roam-dailies-find-tomorrow
      "ordN" 'org-roam-dailies-goto-tomorrow
      "ordd" 'org-roam-dailies-find-directory
      "ordj" 'org-roam-dailies-find-next-note
      "ordJ" 'org-roam-dailies-capture-tomorrow
      "ordk" 'org-roam-dailies-find-yesterday
      "ordK" 'org-roam-dailies-capture-yesterday
      "ord" 'org-roam-dailies-calendar-mark-entries
      "orf" 'org-roam-node-find
      "ors" 'org-roam-db-sync
      "orx" 'org-roam-capture)

    ;; org-roam-mode
    (local-leader-key-def
      :states '(normal visual)
      :keymaps 'org-roam-mode-map
      "ora" '(:ignore t :which-key "Org Roam Alias Prefix")
      "oraa" 'org-roam-alias-add
      "orar" 'org-roam-alias-remove
      "orta" 'org-roam-tag-add
      "ortr" 'org-roam-tag-remove
      "orR" 'org-roam-refile
      "orra" 'org-roam-ref-add
      "orr" '(:ignore t :which-key "Org Roam Ref Prefix")
      "orrf" 'org-roam-ref-find
      "orrh" 'org-roam-ref-history
      "orrr" 'org-roam-ref-remove
      "orT" 'org-roam-bibtex-mode
      "ort" '(:ignore t :which-key "Org Roam Tag Prefix")
      "orta" 'org-roam-tag-add
      "ortr" 'org-roam-tag-remove))

  ;; projectile
  (when +projectile-enabled
    (leader-key-def
      :states '(normal visual)
      "p" (general-simulate-key "C-c p"
            :name +projectile-prefix
            :which-key "Projectile prefix")))
  ;; quit
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "qq" 'save-buffers-kill-terminal
    "qr" 'restart-emacs
    "qR" '+reload-config)

  ;; registers / bookmarks / macros
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "r SPC" 'point-to-register
    "r +" 'increment-register
    "rb" 'bookmark-jump
    "rc" 'copy-to-register
    "rn" 'number-to-register
    "ri" 'insert-register
    "rx" 'copy-to-register
    "rm" 'bookmark-set
    "rM" '(:ignore t :which-key "Macro Prefix")
    "rM RET" 'kmacro-edit-macro
    "rM SPC" 'kmacro-step-edit-macro
    "rM TAB" 'kmacro-insert-counter
    "rM <f3>" 'kmacro-start-macro-or-insert-counter
    "rM <f4>" 'kmacro-end-or-call-macro
    "rM C-d" 'kmacro-delete-ring-head
    "rM C-e" 'kmacro-edit-macro-repeat
    "rM C-k" 'kmacro-end-or-call-macro-repeat
    "rM C-l" 'kmacro-call-ring-2nd-repeat
    "rM C-n" 'kmacro-cycle-ring-next
    "rM C-p" 'kmacro-cycle-ring-previous
    "rM C-v" 'kmacro-view-macro-repeat
    "rM (" 'kmacro-start-macro
    "rM )" 'kmacro-end-macro
    "rMa" 'kmacro-add-counter
    "rMb" 'kmacro-bind-to-key
    "rMc" 'kmacro-set-counter
    "rMe" 'edit-kbd-macro
    "rMf" 'kmacro-set-format
    "rMl" 'kmacro-edit-lossage
    "rMn" 'kmacro-name-last-macro
    "rMt" 'kmacro-swap-ring
    "rMq" 'kbd-macro-query
    "rMx" 'kmacro-to-register
    "rj" 'jump-to-register)

  (when +completion-uses-vertico
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "rb" 'consult-bookmark
      "rc" 'consult-register-store
      "rg" 'consult-global-mark
      "ri" 'consult-register-load
      "rr" 'consult-register))

  ;; search
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "sf" '(:ignore t :which-key "Find prefix")
    "sfF" 'find-function-at-point
    "sfg" 'grep
    "sfh" 'helpful-at-point
    "sfH" 'highlight-symbol-at-point
    "sfL" 'find-library
    "sfv" 'find-variable-at-point
    "sfu" 'unhighlight-regexp)

  (when +completion-uses-vertico
    (leader-key-def
      :keymaps 'override
      :states '(normal visual)
      "sff" 'consult-find
      "sfg" 'consult-grep
      "sfG" 'consult-git-grep
      "sfl" 'consult-locate
      "sh" 'consult-history
      "si" 'consult-imenu
      "sI" 'consult-imenu-multi
      "so" 'consult-outline
      "ss" 'consult-line
      "sS" '+consult/search-symbol-at-point)

    (when +flycheck-enabled
      (leader-key-def
        :states '(normal visual)
        "sfc" 'consult-flycheck
        ))

    (when (executable-find "rg")
      (leader-key-def
        :states '(normal visual)
        "sr" 'consult-ripgrep)))

  ;; tabs
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "tc" 'tab-new
    "tR" 'tab-rename
    "td" 'tab-close
    "tn" 'tab-next
    "tp" 'tab-previous)

  ;; treemacs
  (when +treemacs-enabled
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "tr" 'treemacs))

  (general-define-key
   :keymaps 'treemacs-mode-map
   :prefix "C-c"
   "t" '(:ignore t :which-key "Treemacs Mode Prefix")
   "ta" 'treemacs-create-workspace
   "td" 'treemacs-remove-workspace
   "te" 'treemacs-edit-workspaces
   "tn" 'treemacs-next-workspace
   "tR" 'treemacs-rename-workspace
   "ts" 'treemacs-switch-workspace
   "tk" 'treemacs-root-up
   "tj" 'treemacs-root-down
   "tW" 'treemacs-add-bookmark
   "tg" 'treemacs-refresh
   "tr" 'revert-buffer
   "t?" 'treemacs-advanced-helpful-hydra
   "t<" 'treemacs-decrease-width
   "t>" 'treemacs-increase-width
   "t=" 'treemacs-fit-window-width)

  ;; vterm
  (when +vterm-enabled
    (leader-key-def
      :states '(normal visual)
      :keymaps 'override
      "tv" 'vterm))

  ;; window
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "wo" 'other-window
    "w=" 'balance-windows
    "w+" 'maximize-window
    "w-" 'minimize-window
    "wd" 'delete-window
    "wj" 'evil-window-down
    "wh" 'evil-window-left
    "wH" 'evil-window-top-left ; C-w C-t
    "wj" 'evil-window-down
    "wk" 'evil-window-up
    "wl" 'evil-window-right
    "wL" 'evil-window-bottom-right ; C-w C-b
    "ws" 'evil-window-split ; C-w r
    "wR" 'evil-window-rotate-upwards ; C-w R
    "wr" 'evil-window-rotate-downwards ; C-w r
    "wv" 'evil-window-vsplit ; C-w r
    "ww" 'ace-window
    "wW" 'evil-window-prev ; C-w W
    "w0" 'ace-delete-window
    "w1" 'delete-other-windows)

  ;; mode keybinding
  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "l" '(:ignore t :which-key "Links")
    "n" '(:ignore t :which-key "Narrow")
    "o" '(:ignore t :which-key "Outline")
    "r" '(:ignore t :which-key "Refile")
    "v" '(:ignore t :which-key "Org Babel Prefix")
    "$" 'org-archive-subtree
    "%" 'org-mark-ring-push
    "*" 'org-ctrl-c-star
    "+" 'org-table-sum
    "/" 'org-sparse-tree
    "," 'org-priority
    "-" 'org-ctrl-c-minus
    "." 'org-time-stamp
    "'" 'org-edit-special
    ":" 'org-toggle-fixed-width
    ";" 'org-toggle-comment
    "<" 'org-date-from-calendar
    "=" 'org-table-eval-formula
    ">" 'org-goto-calendar
    "?" 'org-table-field-info
    "@" 'org-mark-subtree
    "[" 'org-agenda-file-to-front
    "\\" 'org-match-sparse-tree
    "]" 'org-ref-helm-insert-cite-link
    "^" 'org-sort
    "`" 'org-table-edit-field
    "|" 'org-force-self-insert
    "C-#" 'org-table-rotate-recalc-marks
    "C-'" 'org-cycle-agenda-files
    "C-," 'org-cycle-agenda-files
    "C-*" 'org-list-make-subtree
    "C-," 'org-insert-structure-template
    "M-h" 'org-mark-element
    "M-{" 'org-backward-element
    "M-}" 'org-forward-element
    "C-a" 'org-attach
    "C-b" '(org-backward-heading-same-level :properties (:repeat t :jump t))
    "C-d" 'org-deadline
    "C-e" 'org-export-dispatch
    "C-f" 'org-forward-heading-same-level
    "C-j" 'org-todo
    "C-w" 'org-refile
    "C-y" 'org-evaluate-time-range
    "C-z" 'org-add-note
    "C-^" 'org-up-element
    "C-_" 'org-down-element
    "C-!" 'org-time-stamp-inactive)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "mll" 'org-insert-link)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "l C-M-l" 'org-insert-all-links
    "l M-l" 'org-insert-last-stored-link
    "l C-n" 'org-next-link
    "l C-p" 'org-previous-link)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "nb" 'org-narrow-to-block
    "ne" 'org-narrow-to-element
    "ns" 'org-narrow-to-subtree)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "o TAB" 'outline-show-children
    "o RET" 'outline-insert-heading
    "o C-a" 'outline-show-all
    "o C-b" 'outline-backward-same-level
    "o C-c" 'outline-hide-entry
    "o C-d" 'outline-hide-subtree
    "o C-f" 'outline-forward-same-level
    "o C-k" 'outline-show-branches
    "o C-l" 'outline-hide-leaves
    "o C-n" 'outline-next-visible-heading
    "o C-o" 'outline-hide-other
    "o C-p" 'outline-previous-visible-heading
    "o C-q" 'outline-hide-sublevels
    "o C-s" 'outline-show-subtree
    "o C-t" 'outline-hide-body
    "o C-u" 'outline-up-heading
    "o C-v" 'outline-move-subtree-down
    "o C-^" 'outline-move-subtree-up
    "o C-@" 'outline-mark-subtree
    "o C-<" 'outline-promote
    "o C->" 'outline-demote)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "rw" 'org-refile
    "rW" 'org-refile-copy
    "r C-M-w" 'org-refile-reverse)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'org-mode-map
    "v C-M-h" 'org-babel-mark-block
    "v C-a" 'org-babel-sha1-hash
    "v C-b" 'org-babel-execute-buffer
    "v C-c" 'org-babel-check-src-block
    "v C-d" 'org-babel-demarcate-block
    "v C-e" 'org-babel-execute-maybe
    "v C-f" 'org-babel-tangle-file
    "v TAB" 'org-babel-view-src-block-info
    "v C-j" 'org-babel-insert-header-arg
    "v C-l" 'org-babel-load-in-session
    "v C-n" 'org-babel-next-src-block
    "v C-o" 'org-babel-open-src-block-result
    "v C-p" 'org-babel-previous-src-block
    "v C-r" 'org-babel-goto-named-result
    "v C-s" 'org-babel-execute-subtree
    "v C-t" 'org-babel-tangle
    "v C-u" 'org-babel-goto-src-block-head
    "v C-v" 'org-babel-expand-src-block
    "v C-x" 'org-babel-do-key-sequence-in-edit-buffer
    "v C-z" 'org-babel-switch-to-session
    "vI" 'org-babel-view-src-block-info
    "va" 'org-babel-sha1-hash
    "vb" 'org-babel-execute-buffer
    "vc" 'org-babel-check-src-block
    "vd" 'org-babel-demarcate-block
    "ve" 'org-babel-execute-maybe
    "vf" 'org-babel-tangle-file
    "vg" 'org-babel-goto-named-src-block
    "vh" 'org-babel-describe-bindings
    "vi" 'org-babel-lob-ingest
    "vj" 'org-babel-insert-header-arg
    "vk" 'org-babel-remove-result-one-or-many
    "vl" 'org-babel-load-in-session
    "vn" 'org-babel-next-src-block
    "vo" 'org-babel-open-src-block-result
    "vp" 'org-babel-previous-src-block
    "vr" 'org-babel-goto-named-result
    "vs" 'org-babel-execute-subtree
    "vt" 'org-babel-tangle
    "vu" 'org-babel-goto-src-block-head
    "vv" 'org-babel-expand-src-block
    "vx" 'org-babel-do-key-sequence-in-edit-buffer
    "vz" 'org-babel-switch-to-session-with-code)

  ;; flycheck
  (leader-key-def
    :states '(normal visual)
    :keymaps '(prog-mode-map)
    "c?" 'flycheck-describe-checker
    "cb" 'flycheck-buffer
    "cc" 'flycheck-compile
    "c C-c" 'flycheck-compile
    "c C-w" 'flycheck-copy-errors-as-kill
    "c C-p" 'flycheck-next-error
    "cC" 'flycheck-clear
    "cH" 'display-local-help
    "ci" 'flycheck-manual
    "cl" 'flycheck-list-errors
    "cn" 'flycheck-next-error
    "cp" 'flycheck-next-error
    "cs" 'flycheck-select-checker
    "cv" 'flycheck-verify-setup
    "cV" 'flycheck-version
    "cx" 'flycheck-disable-checker)

  ;; completion
  (leader-key-def
    :states '(normal visual)
    :keymaps '(prog-mode-map text-mode-map)
    "/" '(:ignore t :which-key "Completion")
    "//" 'completion-at-point
    "/t" 'complete-tag
    "/d" 'cape-dabbrev
    "/h" 'cape-history
    "/f" 'cape-file
    "/k" 'cape-keyword
    "/s" 'cape-symbol
    "/a" 'cape-abbrev
    "/i" 'cape-ispell
    "/l" 'cape-line
    "/w" 'cape-dict
    "/\\" 'cape-tex
    "/_" 'cape-tex
    "/^" 'cape-tex
    "/&" 'cape-sgml
    "/r" 'cape-rfc1345)

  ;; elisp-mode
  ;; visual state and active region
  (local-leader-key-def
    :states 'visual
    :keymaps 'emacs-lisp-mode-map
    :predicate '(region-active-p)
    "er" 'eval-region)

  ;; normal + visual state
  (local-leader-key-def
    :states '(normal visual)
    :keymaps 'emacs-lisp-mode-map
    "e" '(:ignore t :which-key "Eval Prefix")
    "eb" 'eval-buffer
    "ed" 'eval-defun
    "eR" 'xref-find-references)

  ;; geiser
  (general-define-key
   :states '(normal visual insert emacs)
   :keymaps '(geiser-mode-map scheme-mode-map racket-mode-map)
   "C-." 'geiser-completion--complete-module
   "C-M-i" 'completion-at-point
   "C-x C-e" 'geiser-eval-last-sexp
   "M-," 'geiser-pop-symbol-stack
   "M-." 'geiser-edit-symbol-at-point
   "M-`" 'geiser-completion--complete-module)

  (local-leader-key-def
    :states 'visual
    :keymaps '(geiser-mode-map scheme-mode-map racket-mode-map)
    :predicate '(region-active-p)
    "gr" 'geiser-eval-region
    "gR" 'geiser-eval-region-and-go
    "g C-r" 'geiser-expand-region)

  ;; eval
  (local-leader-key-def
    :states '(normal visual)
    :keymaps '(geiser-mode-map scheme-mode-map racket-mode-map)
    "e" '(:ignore t :which-key "Eval Prefix")
    "ed" 'geiser-eval-definition
    "eb" 'geiser-eval-buffer
    "ec" 'geiser-eval-definition
    "e TAB" 'geiser-eval-interrupt
    "ei" 'geiser-eval-interrupt
    "eB" 'geiser-eval-buffer-and-go
    "ee" 'geiser-eval-definition-and-go
    "e C-TAB" 'geiser-eval-interrupt)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps '(geiser-mode-map scheme-mode-map racket-mode-map)
    "g" '(:ignore t :which-key "Geiser Prefix")
    "g<" 'geiser-xref-callers
    "g>" 'geiser-xref-callees
    "ga" 'geiser-mode-switch-to-repl-and-enter
    "g C-a" 'geiser-autodoc-mode
    "g C-d" 'geiser-doc-symbol-at-point
    "g C-s" 'geiser-autodoc-show
    "g RET" 'geiser-doc-module
    "g TAB" 'geiser-doc-look-up-manual
    "g C-l" 'geiser-add-to-load-path
    "g ESC" 'geiser-squarify
    "g C-e RET" 'geiser-edit-module
    "g C-q" 'geiser-exit-repl
    "g C-z" 'geiser-mode-switch-to-repl
    "gg" 'run-geiser
    "gr" 'geiser
    "g[" 'geiser-squarify
    "gl" 'geiser-load-file
    "gm" 'geiser-edit-module
    "gk" 'geiser-compile-current-buffer
    "gs" 'geiser-set-scheme
    "gS" 'geiser-expand-last-sexp
    "g C-RET" 'geiser-expand-definition
    "g\\" 'geiser-insert-lambda)

  ;; common-lisp
  ;; sly-mode

  ;; region
  (local-leader-key-def
    :states 'visual
    :keymaps 'sly-mode-map
    :predicate '(region-active-p)
    "g" '(:ignore t :which-key "Common lisp Region Prefix")
    "gr" 'sly-eval-region
    "gc" 'sly-compile-region
    "gp" 'sly-pprint-eval-region
    "gC" 'sly-stickers-clear-region-stickers)

  ;; eval
  (local-leader-key-def
    :states '(normal visual)
    :keymaps '(sly-mode-map)
    "e" '(:ignore t :which-key "Eval Prefix")
    "eb" 'sly-eval-buffer
    "ed" 'sly-db-eval-in-frame
    "ex" 'sly-eval-defun
    "ep" 'sly-pprint-eval-last-expression
    "ei" 'sly-inspector-eval
    "eI" 'sly-interactive-eval
    "e C-e" 'sly-eval-last-expression
    "eP" 'sly-eval-print-last-expression
    "em" 'sly-eval-macroexpand-inplace)

  ;; misc
  (local-leader-key-def
    :states '(normal visual)
    :keymaps '(sly-mode-map)
    "s" '(:ignore t :which-key "Sly Prefix")
    "s." 'sly-edit-definition
    "s C-." 'sly-edit-definition-other-window
    "s M-." 'sly-edit-definition-other-frame
    "s?" 'sly-edit-uses-xrefs
    "s_" 'sly-edit-uses
    "s<" 'sly-list-callers
    "s>" 'sly-list-callees
    "s#" 'common-lisp-hyperspec-lookup-reader-macro
    "s RET" 'sly-expand-1
    "s C-RET" 'sly-expand-1-inplace
    "s C-f" 'sly-describe-function
    "s C-d" 'sly-describe-symbol
    "s C-z" 'sly-mrepl
    "s C-p" 'sly-prev-connection
    "s C-n" 'sly-next-connection
    "s C-u" 'sly-undefine-function
    "s C-t" 'sly-toggle-fancy-trace
    "s C-~" 'common-lisp-hyperspec-format
    "s C-c C-z" 'sly-apropos-all
    "s C-c C-z" 'sly-apropos-package
    "s C-c C-g" 'common-lisp-hyperspec-glossary-term
    "s C-h" 'sly-documentation-lookup
    "sc" 'sly-list-connections
    "sd" 'sly-disassemble-symbol
    "sD" 'sly-disassemble-definition
    "se" 'sly-edit-value
    "sc" 'sly-connect
    "sx" 'sly-export-symbol-at-point
    "si" 'sly-import-symbol-at-point
    "sI" 'sly-inspect
    "sl" 'sly-load-file
    "sm" 'sly-macroexpand-1
    "sM" 'sly-macroexpand-all
    "st" 'sly-list-threads
    "sw" 'sly-who-macroexpands
    "s C-a" 'sly-who-specializes
    "s C-b" 'sly-who-binds
    "s C-s" 'sly-who-sets
    "s C-w" 'sly-calls-who
    "s C-r" 'sly-who-references
    "s M-w" 'sly-who-calls)

  ;; notmuch
  (leader-key-def
    :states '(normal visual)
    :keymaps 'override
    "am" 'notmuch)

  (local-leader-key-def
    :states '(normal visual)
    :keymaps '(notmuch-hello-mode-map notmuch-search-mode-map)
    "r" 'notmuch-refresh-this-buffer
    "R" 'notmuch-poll-and-refresh-this-buffer
    "A" 'notmuch-refresh-all-buffers
    "k" 'notmuch-tag-jump)
  )

(provide 'config-keybindings)
;;; config-keybindings.el ends here

;;; config-packages.el --- package variables and helper functions -*- lexical-binding: t -*-
;;; Commentary:
;;
;;; Code:

;; essential packages
(+install-package 'no-littering)
(+install-package 'which-key)
(which-key-mode)
(setq which-key-sort-order #'which-key-key-order-alpha
      which-key-sort-uppercase-first nil
      which-key-add-column-padding 1
      which-key-max-display-columns nil
      which-key-min-display-lines 6
      which-key-side-window-slot -10)
(define-key which-key-mode-map (kbd "C-x <f5>") 'which-key-C-h-dispatch)
(which-key-setup-side-window-bottom)
(+install-package 'diminish)
(+install-package 'elisp-demos)
(+install-package 'helpful)

(require 'no-littering)
(require 'which-key)
(require 'diminish)
(require 'windmove)

(add-to-list '+diminished-list 'which-key-mode)

(+install-package 'scratch-el 'scratch)
(require 'scratch)
(define-key (current-global-map) (kbd "C-c S") #'scratch)

(when (executable-find "git")
  (+install-package 'magit)
  (+install-package 'transient)
  (+install-package 'diff-hl)
  (setq +diff-hl-enabled t)
  (require 'magit)
  (require 'diff-hl)
  (transient-append-suffix 'magit-log "a"
    '("w" "Wip" magit-wip-log-current))
  (transient-append-suffix 'magit-log "-A"
    '("-m" "Omit merge commits" "--no-merges"))
  (setq magit-log-arguments '("-n100" "--graph" "--decorate"))
  (add-hook 'magit-pre-refresh-hook #'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)
  (add-hook 'magit-mode-hook #'hl-line-mode)
  (when (featurep 'treemacs)
    (+install-package 'treemacs-magit)
    (require 'treemacs-magit)))

;; ace window
(+install-package 'ace-window)
(customize-set-variable 'aw-scope 'frame)
(customize-set-variable 'aw-dispatch-always nil)
(customize-set-variable 'aw-minibuffer-flag t)
(customize-set-variable 'aw-keys '(?q ?w ?e ?r ?t ?a ?s ?d ?f))
(defvar aw-dispatch-alist
  '((?x aw-delete-window "Delete Window")
    (?m aw-swap-window "Swap Windows")
    (?M aw-move-window "Move Window")
    (?c aw-copy-window "Copy Window")
    (?j aw-switch-buffer-in-window "Select Buffer")
    (?n aw-flip-window)
    (?u aw-switch-buffer-other-window "Switch Buffer Other Window")
    (?c aw-split-window-fair "Split Fair Window")
    (?v aw-split-window-vert "Split Vert Window")
    (?b aw-split-window-horz "Split Horz Window")
    (?o delete-other-windows "Delete Other Windows")
    (?? aw-show-dispatch-help))
  "List of actions for `aw-dispatch-default'.")
(require 'ace-window )
(set-face-attribute
 'aw-leading-char-face nil
 :foreground "deep sky blue"
 :weight 'bold
 :height 3.0)
(set-face-attribute
 'aw-mode-line-face nil
 :inherit 'mode-line-buffer-id
 :foreground "lawn green")
(global-set-key [remap other-window] 'ace-window)
(ace-window-display-mode t)

;; perspective
(when +perspective-enabled
  (setq persp-initial-frame-name "Main"
        persp-suppress-no-prefix-key-warning t
        persp-mode-prefix-key (kbd "C-c TAB")
        persp-state-default-file (expand-file-name "var/.perspective-state" +emacs-data-dir))
  (+install-package 'perspective)
  (unless (equal persp-mode t)
    (persp-mode 1))
  (global-set-key [remap switch-to-buffer] #'persp-switch-to-buffer*)
  (define-key persp-mode-map (kbd "C-c TAB TAB") 'persp-switch-last)
  (unless (string= "guix" +distro)
    (add-hook 'emacs-startup-hook (lambda () (persp-state-load persp-state-default-file)))
    (add-hook 'kill-emacs-hook #'persp-state-save))
  (when (featurep 'treemacs)
    (require 'treemacs-perspective))
  (when (featurep 'consult)
    (require 'consult)
    (unless (boundp 'persp-consult-source)
      (defvar persp-consult-source
        (list :name     "Perspective"
              :narrow   ?s
              :category 'buffer
              :state    #'consult--buffer-state
              :history  'buffer-name-history
              :default  t
              :items
              #'(lambda () (consult--buffer-query :sort 'visibility
                                                  :predicate '(lambda (buf) (persp-is-current-buffer buf t))
                                                  :as #'buffer-name)))))
    (consult-customize consult--source-buffer :hidden t :default nil)
    (add-to-list 'consult-buffer-sources persp-consult-source)))

;; rainbow-*
(when +rainbow-enabled
  (+install-package 'rainbow-mode)
  (+install-package 'rainbow-delimiters)
  (+install-package 'rainbow-identifiers)
  ;; rainbow-mode
  (add-hook 'css-mode-hook #'rainbow-mode)
  ;; turn off word colors
  ;; TODO: set toggling keybinding
  (defun +rainbow-turn-off-words ()
    "Turn off word colors in rainbow-mode."
    (interactive)
    (font-lock-remove-keywords
     nil
     `(,@rainbow-x-colors-font-lock-keywords
       ,@rainbow-latex-rgb-colors-font-lock-keywords
       ,@rainbow-r-colors-font-lock-keywords
       ,@rainbow-html-colors-font-lock-keywords
       ,@rainbow-html-rgb-colors-font-lock-keywords)))
  ;; turn off hexadecimal colors
  (defun +rainbow-turn-off-hexadecimal ()
    "Turn off hexadecimal colors in rainbow-mode."
    (interactive)
    (font-lock-remove-keywords
     nil
     `(,@rainbow-hexadecimal-colors-font-lock-keywords)))
  ;; rainbow-delimiters
  (require 'rainbow-delimiters)
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'org-mode-hook 'rainbow-delimiters-mode)
  (add-to-list '+diminished-list 'rainbow-mode)
  ;; rainbow-identifiers
  (add-hook 'prog-mode-hook 'rainbow-identifiers-mode))

;; vertico-posframe
(when +vertico-posframe-enabled
  (+install-package 'vertico-posframe)
  (with-eval-after-load 'vertico
    (unless (daemonp)
      (require 'vertico-posframe)
      (vertico-posframe-mode 1)))
  (when (featurep 'corfu)
    (unless (daemonp)
      (setq corfu-preview-current nil
            corfu-echo-documentation nil))))

;; hl-todo
(when +hl-todo-enabled
  (+install-package 'hl-todo)
  (require 'hl-todo)
  (setq hl-todo-wrap-movement t)
  (global-hl-todo-mode))

;; diff-hl
(when +diff-hl-enabled
  (+install-package 'diff-hl)
  (require 'diff-hl)
  (global-diff-hl-mode)
  (diff-hl-dir-mode)
  (when (featurep 'flycheck)
    (setq flycheck-indication-mode 'right-fringe))
  (when (featurep 'magit)
    (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
    (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)))

;; treemacs
;; treemacs-projectile, treemacs-magit, treemacs-icons-dired
;; treemacs-perspective, treemacs-all-the-icons
;; are included in `emacs-treemacs-extra' package.
(when +treemacs-enabled
  (+install-package 'treemacs)
  (+install-package 'treemacs-extra 'treemacs-projectile)
  (require 'treemacs)
  (require 'treemacs-peek-mode)
  (setq treemacs-hide-gitignored-files-mode t
        treemacs-silent-refresh t
        treemacs-sorting 'mod-time-desc)
  (treemacs-peek-mode 1)
  (require 'treemacs-icons-dired)
  (when (display-graphic-p)
    (require 'treemacs-all-the-icons)))

;; multiple-cursors
;; TODO: configure `emacs-mc-extras' and
;; `emacs-evil-mc'.
(when +multiple-cursors-enabled
  (+install-package 'multiple-cursors)
  (+install-package 'mc-extras)
  ;; (when +evil-enabled
  ;;   (+install-package 'evil-mc))
  (require 'multiple-cursors)
  (setq mc/list-file (expand-file-name ".mc-lists.el" +emacs-data-dir))
  (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
  (global-set-key (kbd "C-c M->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-c M-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c M-a") 'mc/mark-all-like-this))

;; yasnippet
(when +yasnippet-enabled
  (+install-package 'yasnippet)
  (+install-package 'yasnippet-snippets)
  (when +completion-uses-vertico
    (+install-package 'consult-yasnippet))
  (yas-global-mode 1)
  (append '(yas-global-mode yas-minor-mode) +diminished-list)
  (global-set-key (kbd "C-c y i") 'yas-insert-snippet)
  (global-set-key (kbd "C-c y d") 'yas-describe-tables))

;; yaml-mode
(when +yaml-enabled
  (+install-package 'yaml)
  (+install-package 'yaml-mode)
  (add-to-list 'auto-mode-alist '("'\\.yaml\\'" . yaml-mode))
  (add-to-list 'auto-mode-alist '("'\\.yml\\'" . yaml-mode))
  (add-hook 'yaml-mode-hook
            #'(lambda ()
                (define-key yaml-mode-map "\C-m" 'newline-and-indent))))

;; toml-mode
(when +toml-mode-enabled
  (+install-package 'toml-mode)
  (add-to-list 'auto-mode-alist '("'\\.toml\\'" . toml-mode)))

;; completion
(when +completion-uses-vertico
  (+install-package 'vertico)
  (+install-package 'orderless)
  (+install-package 'marginalia)
  (+install-package 'consult)
  (+install-package 'consult-dir)
  (+install-package 'embark)
  (+install-package 'corfu)
  (+install-package 'corfu-doc)
  (+install-package 'kind-icon)
  (+install-package 'cape)

  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)
  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  (setq tab-always-indent 'complete)
  (customize-set-variable 'vertico-cycle t)
  (require 'vertico)
  (vertico-mode)
  ;; Use `consult-completion-in-region' if Vertico is enabled.
  ;; Otherwise use the default `completion--in-region' function.
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args)))
  ;; tmm-menubar
  (global-set-key [f10] #'tmm-menubar)
  (advice-add #'tmm-add-prompt :after #'minibuffer-hide-completions)
  ;; ffap-menu
  (advice-add #'ffap-menu-ask :around (lambda (&rest args)
                                        (cl-letf (((symbol-function #'minibuffer-completion-help)
                                                   #'ignore))
                                          (apply args))))
  ;; prefix
  (advice-add #'vertico--format-candidate :around
              (lambda (orig cand prefix suffix index _start)
                (setq cand (funcall orig cand prefix suffix index _start))
                (concat
                 (if (= vertico--index index)
                     (propertize "» " 'face 'vertico-current)
                   "  ")
                 cand)))
  ;; don't let vertico resize the minibuffer
  (setq vertico-resize nil)
  ;; increase the number of candidates to show
  (setq vertico-count 15)
  (require 'vertico-multiform)
  (setq vertico-multiform-commands
        '((consult-imenu buffer indexed)
          (consult-imenu-multi buffer indexed)))
  (setq vertico-multiform-categories
        '((symbol (vertico-sort-function . vertico-sort-alpha))
          (file (vertico-sort-function . sort-directories-first))))
  (setq vertico-multiform-categories
        '((buffer flat (vertico-cycle . t))))
  (defun sort-directories-first (files)
    (setq files (vertico-sort-history-length-alpha files))
    (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
           (seq-remove (lambda (x) (string-suffix-p "/" x)) files)))
  (global-set-key (kbd "<C-S-iso-lefttab>") #'vertico-repeat)
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
  (require 'vertico-directory)
  (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
  (define-key vertico-map (kbd "DEL") 'vertico-directory-delete-char)
  ;; orderless
  (require 'orderless)
  (setq completion-styles '(basic partial-completion orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles basic partial-completion))))
  ;; marginalia
  (require 'marginalia)
  (define-key minibuffer-local-map (kbd "M-A") 'marginalia-cycle)
  (marginalia-mode 1)
  ;; consult
  (require 'consult)
  (add-hook 'completion-list-mode-hook #'consult-preview-at-point-mode)
  (advice-add #'register-preview :override #'consult-register-window)
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  (consult-customize
   consult-theme
   :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-recent-file
   consult--source-project-recent-file
   :preview-key (kbd "M-."))
  (setq consult-narrow-key "<") ;; (kbd "C-+")
  ;; remap keybindings
  (global-set-key [remap apropos-command] #'consult-apropos)
  (global-set-key [remap bookmark-jump] #'consult-bookmark)
  (global-set-key [remap goto-line] #'consult-goto-line)
  (global-set-key [remap imenu] #'consult-imenu)
  (global-set-key [remap locate] #'consult-locate)
  (global-set-key [remap load-theme] #'consult-theme)
  (global-set-key [remap man] #'consult-man)
  (global-set-key [remap recentf-open-files] #'consult-recent-file)
  (global-set-key [remap repeat-complex-command] #'consult-complex-command)
  (global-set-key [remap project-switch-to-buffer] #'consult-project-buffer)
  (global-set-key [remap switch-to-buffer] #'consult-buffer)
  (global-set-key [remap switch-to-buffer-other-window] #'consult-buffer-other-window)
  (global-set-key [remap switch-to-buffer-other-frame] #'consult-buffer-other-frame)
  (global-set-key [remap yank-pop] #'consult-yank-pop)
  ;; Isearch integration
  (define-key isearch-mode-map [remap isearch-edit-string] #'consult-isearch-history)
  (global-set-key (kbd "M-s e") 'consult-isearch-history)
  (define-key isearch-mode-map (kbd "M-s l") #'consult-line) ;; needed by consult-line to detect isearch
  (define-key isearch-mode-map (kbd "M-s L") #'consult-line-multi) ;; needed by consult-line to detect
  ;;global
  (global-set-key (kbd "C-c h") 'consult-history)
  (global-set-key (kbd "C-c m") 'consult-mode-command)
  (global-set-key (kbd "C-c k") 'consult-kmacro)
  ;; Custom M-# bindings for fast register access
  (global-set-key (kbd "M-#") 'consult-register-load)
  (global-set-key (kbd "M-'") 'consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
  (global-set-key (kbd "C-M-#") 'consult-register)
  ;; M-g bindings (goto-map)
  (global-set-key (kbd "M-g e") 'consult-compile-error)
  (global-set-key (kbd "M-g o") 'consult-outline)               ;; Alternative: consult-org-heading
  ;;(global-set-key (kbd "M-g m") 'consult-mark) ;;conflict with telega
  (global-set-key (kbd "M-g k") 'consult-global-mark)
  ;;(global-set-key (kbd "M-g i") 'consult-imenu) ;; conflict with telega
  (global-set-key (kbd "M-g I") 'consult-imenu-multi)
  ;; M-s bindings (search-map)
  (global-set-key (kbd "M-s d") 'consult-find)
  (global-set-key (kbd "M-s D") 'consult-locate)
  (global-set-key (kbd "M-s g") 'consult-grep)
  (global-set-key (kbd "M-s G") 'consult-git-grep)
  (global-set-key (kbd "M-s r") 'consult-ripgrep)
  (global-set-key (kbd "M-s l") 'consult-line)
  (global-set-key (kbd "M-s L") 'consult-line-multi)
  (global-set-key (kbd "M-s m") 'consult-multi-occur)
  (global-set-key (kbd "M-s k") 'consult-keep-lines)
  (global-set-key (kbd "M-s u") 'consult-focus-lines)
  ;; flycheck/flymake
  (when (featurep 'flymake)
    (global-set-key (kbd "M-g f") 'consult-flymake)) ;; Alternative: consult-flycheck
  (when (featurep 'flycheck)
    (global-set-key (kbd "M-g f") 'consult-flycheck))
  (global-set-key [remap list-directory] #'consult-dir)
  (cond ((featurep 'vertico)
         (define-key vertico-map (kbd "C-x C-d") 'consult-dir)
         (define-key vertico-map (kbd "C-x C-j") 'consult-dir-jump-file))
        ((featurep 'selectrum)
         (define-key selectrum-minibuffer--map (kbd "C-x C-d") 'consult-dir)
         (define-key selectrum-minibuffer-map (kbd "C-x C-j") 'consult-dir-jump-file)))
  (define-key minibuffer-local-completion-map (kbd "C-x C-d") 'consult-dir)
  (define-key minibuffer-local-completion-map (kbd "C-x C-j") 'consult-dir-jump-file)
  ;; project
  (cond
   ((featurep 'projectile)
    (autoload 'projectile-project-root "projectile")
    (setq consult-project-root-function #'projectile-project-root))
   ((featurep 'project)
    (setq consult-project-root-function #'consult--project-root-default-function))
   ((featurep 'vc)
    (setq consult-project-root-function #'vc-root-dir)))
  ;; custom function
  (defun +consult/search-symbol-at-point ()
    (interactive)
    (consult-line (thing-at-point 'symbol)))
  ;; embark
  (require 'embark)
  (require 'embark-consult)
  (require 'embark-org)
  (global-set-key [remap describe-bindings] #'embark-bindings)
  (global-set-key (kbd "C-c C-.") 'embark-act)
  (setq prefix-help-command #'embark-prefix-help-command)
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  (when (featurep 'which-key)
    (setq which-key-use-C-h-commands nil
          prefix-help-command #'embark-prefix-help-command))
  ;; corfu
  (require 'corfu)
  (require 'corfu-history)
  (require 'corfu-indexed)
  (require 'corfu-info)
  (require 'corfu-quick)
  ;;(customize-set-variable 'corfu-cycle t)
  (customize-set-variable 'corfu-auto t)
  ;;(customize-set-variable 'corfu-preselect-first t)
  (customize-set-variable 'corfu-quit-no-match 'separator)
  ;;(customize-set-variable 'corfu-separator ?\s)
  (setq completion-cycle-threshold 3
        tab-always-indent 'complete)
  (defun +corfu-enable-in-minibuffer ()
    "Enable Corfu in the minibuffer if `completion-at-point' is bound."
    (when (where-is-internal #'completion-at-point (list (current-local-map)))
      ;; (setq-local corfu-auto nil) Enable/disable auto completion
      (corfu-mode 1)))
  (add-hook 'minibuffer-setup-hook #'+corfu-enable-in-minibuffer)
  (add-hook 'eshell-mode-hook
            (lambda ()
              (setq-local corfu-auto nil)
              (corfu-mode)))
  (define-key corfu-map (kbd "TAB") 'corfu-next)
  (define-key corfu-map (kbd "S-TAB") 'corfu-previous)
  (defun +corfu-beginning-of-prompt ()
    "Move to beginning of completion input."
    (interactive)
    (corfu--goto -1)
    (goto-char (car completion-in-region--data)))
  (defun +corfu-end-of-prompt ()
    "Move to end of completion input."
    (interactive)
    (corfu--goto -1)
    (goto-char (cadr completion-in-region--data)))
  (define-key corfu-map [remap move-beginning-of-line] #'+corfu-beginning-of-prompt)
  (define-key corfu-map [remap move-end-of-line] #'+corfu-end-of-prompt)
  (global-corfu-mode)
  (when (featurep' all-the-icons)
    (require 'kind-icon)
    (customize-set-variable 'kind-icon-default-face 'corfu-default)
    (add-hook 'my-completion-ui-mode-hook
              (lambda ()
                (setq completion-in-region-function
                      (kind-icon-enhance-completion
                       completion-in-region-function)))))
  (add-hook 'corfu-mode-hook #'corfu-doc-mode)
  (require 'corfu-history)
  (require 'savehist)
  (add-to-list 'savehist-additional-variables 'corfu-history)
  ;; cape
  (require 'cape)
  (add-to-list 'completion-at-point-functions #'cape-file)
  ;; (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-history)
  ;; (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;; (add-to-list 'completion-at-point-functions #'cape-tex)
  ;; (add-to-list 'completion-at-point-functions #'cape-sgml)
  ;; (add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;; (add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;; (add-to-list 'completion-at-point-functions #'cape-ispell)
  ;; (add-to-list 'completion-at-point-functions #'cape-dict)
  (add-to-list 'completion-at-point-functions #'cape-symbol)
  ;; (add-to-list 'completion-at-point-functions #'cape-line)
  (global-set-key (kbd "C-c i <return>") 'completion-at-point)
  (global-set-key (kbd "C-c i t") 'complete-tag)
  (global-set-key (kbd "C-c i d") 'cape-dabbrev)
  (global-set-key (kbd "C-c i h") 'cape-history)
  (global-set-key (kbd "C-c i f") 'cape-file)
  (global-set-key (kbd "C-c i k") 'cape-keyword)
  (global-set-key (kbd "C-c i s") 'cape-symbol)
  (global-set-key (kbd "C-c i a") 'cape-abbrev)
  (global-set-key (kbd "C-c i i") 'cape-ispell)
  (global-set-key (kbd "C-c i l") 'cape-line)
  (global-set-key (kbd "C-c i w") 'cape-dict)
  (global-set-key (kbd "C-c i \\") 'cape-tex)
  (global-set-key (kbd "C-c i _") 'cape-tex)
  (global-set-key (kbd "C-c i ^") 'cape-tex)
  (global-set-key (kbd "C-c i &") 'cape-sgml)
  (global-set-key (kbd "C-c i r") 'cape-rfc1345)
  ;; dabbrev
  (require 'dabbrev)
  (global-set-key (kbd "M-/") 'dabbrev-completion)
  (global-set-key (kbd "C-M-/") 'dabbrev-expand)
  (customize-set-variable 'dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'")))

;; no littering
(when (featurep 'no-littering)
  (setq auto-save-file-name-transforms
        `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
  (with-eval-after-load 'recentf
    (progn
      (add-to-list 'recentf-exclude no-littering-var-directory)
      (add-to-list 'recentf-exclude no-littering-etc-directory))))

;; ui
(+install-package 'all-the-icons)
(+install-package 'all-the-icons-ibuffer)
(+install-package 'all-the-icons-dired)
(+install-package 'all-the-icons-completion)
(+install-package 'doom-modeline)
(+install-package 'modus-themes)
(+install-package 'doom-themes)
(+install-package 'theme-magic)

(when (display-graphic-p)
  (require 'all-the-icons)
  (require 'all-the-icons-ibuffer)
  (require 'all-the-icons-completion)
  (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
  (all-the-icons-completion-mode)
  (when (featurep 'marginalia)
    (add-hook 'marginalia-mode-hook #'all-the-icons-completion-marginalia-setup)))

(customize-set-variable 'doom-modeline-height 15)
(customize-set-variable 'doom-modeline-bar-width 6)
(customize-set-variable 'doom-modeline-minor-modes t)
(customize-set-variable 'doom-modeline-buffer-file-name-style 'file-name)
;; (customize-set-variable 'doom-modeline-github t) ; uses `ghub' and `async' package
;; (customize-set-variable 'doom-modeline-indent-info t)
(require 'doom-modeline)
(doom-modeline-mode 1)
(add-hook 'after-init-hook #'doom-modeline-mode)
(add-hook 'doom-modeline-mode-hook #'size-indication-mode)
(add-hook 'doom-modeline-mode-hook #'column-number-mode)
(when (daemonp)
  (setq doom-modeline-icon t))

(require 'doom-themes)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled
(load-theme +config-doom-theme t)
(doom-themes-visual-bell-config)
;; Enable custom neotree theme (all-the-icons must be installed!)
(with-eval-after-load 'neotree
  (doom-themes-neotree-config))
;; or for treemacs users
(with-eval-after-load 'treemacs
  (setq doom-themes-treemacs-theme "doom-colors") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config))
;; Corrects (and improves) org-mode's native fontification.
(doom-themes-org-config)
(when (executable-find "wal")
  (require 'theme-magic)
  (unless (daemonp)
    (add-hook 'after-init-hook #'theme-magic-from-emacs)
    (theme-magic-export-theme-mode)))
(add-to-list '+diminished-list 'theme-magic-export-theme-mode)

;; helpful
(require 'helpful)
(define-key helpful-mode-map [remap revert-buffer] #'helpful-update)
(global-set-key [remap describe-command] #'helpful-command)
(global-set-key [remap describe-function] #'helpful-callable)
(global-set-key [remap describe-key] #'helpful-key)
(global-set-key [remap describe-symbol] #'helpful-symbol)
(global-set-key [remap describe-variable] #'helpful-variable)
(global-set-key (kbd "C-h F") #'helpful-function)
(global-set-key (kbd "C-h K") #'describe-keymap)
;; elisp-demos
(require 'elisp-demos)
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)

(when +flycheck-enabled
  (+install-package 'flycheck)
  (require 'flycheck)
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (let ((chicken-executables
         (cond ((string= "arch"
                         (getenv "DISTRO"))
                "chicken-csc"))))
    (setq flycheck-scheme-chicken-executable chicken-executables))
  (setq flycheck-emacs-lisp-load-path 'inherit)
  (delq 'new-line flycheck-check-syntax-automatically)
  (setq flycheck-buffer-switch-check-intermediate-buffers t
        flycheck-display-errors-delay 0.25)
  (when (featurep 'consult)
    (+install-package 'consult-flycheck)
    (require 'consult-flycheck)))

(add-to-list '+diminished-list 'flycheck-mode)

(when +vterm-enabled
  (+install-package 'vterm)
  (+install-package 'vterm-toggle)
  (require 'vterm)
  (setq vterm-max-scrollback 5000)
  (add-hook 'vterm-mode-hook (lambda () (setq confirm-kill-processes nil
                                              hscroll-margin 0)))
  (setq vterm-kill-buffer-on-exit t
        vterm-copy-exclude-prompt t)
  (require 'vterm-toggle)
  (global-set-key [f2] 'vterm-toggle)
  (global-set-key [C-f2] 'vterm-toggle-cd)
  ;; Switch to next vterm buffer
  (define-key vterm-mode-map (kbd "C-c C-n") 'vterm-toggle-forward)
  ;; Switch to previous vterm buffer
  (define-key vterm-mode-map (kbd "C-c C-p") 'vterm-toggle-backward)
  ;; show vterm buffer in bottom side
  (setq vterm-toggle-fullscreen-p nil)
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                   (let ((buffer (get-buffer buffer-or-name)))
                     (with-current-buffer buffer
                       (or (equal major-mode 'vterm-mode)
                           (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
                 ;; (display-buffer-reuse-window display-buffer-at-bottom)
                 (display-buffer-reuse-window display-buffer-in-direction)
                 ;; display-buffer-in-direction/direction/dedicated is added in emacs27
                 (direction . bottom)
                 (dedicated . t) ;dedicated is supported in emacs27
                 (reusable-frames . visible)
                 (window-height . 0.3)))
  (define-key vterm-mode-map (kbd "<f2>") 'vterm-toggle)
  (define-key vterm-mode-map (kbd "C-<f2>") 'vterm-toggle-cd))

(when +guix-enabled
  (+install-package 'guix)
  (add-hook 'after-init-hook 'global-guix-prettify-mode)
  (add-to-list '+diminished-list 'guix-prettify-mode)
  (add-hook 'shell-mode-hook 'guix-build-log-minor-mode)
  (add-hook 'scheme-mode-hook 'guix-devel-mode)
  (add-to-list '+diminished-list 'guix-devel-mode)
  (with-eval-after-load 'ffap
    (add-to-list 'ffap-alist '("\\.patch" . guix-devel-ffap-patch))))

(when +geiser-enabled
  (+install-package 'geiser)
  (+install-package 'geiser-guile)
  (require 'geiser)
  (require 'geiser-impl)
  (add-to-list
   'geiser-implementations-alist '((regexp "\\.scm$") guile))
  (setq geiser-scheme-implementation "guile"
        geiser-default-implementation "guile")
  (add-to-list '+diminished-list 'geiser-autodoc-mode)
  (add-to-list '+diminished-list 'geiser-mode)
  (when +flycheck-enabled
    (+install-package 'flycheck-guile)))

(when +nix-mode-enabled
  (+install-package 'nix-mode)
  (add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode)))

(when +nginx-mode-enabled
  (+install-package 'nginx-mode)
  (add-to-list 'auto-mode-alist '("/nginx/sites-\\(?:available\\|enabled\\)/" . nginx-mode)))

(when +markdown-enabled
  (+install-package 'markdown-mode)
  (+install-package 'markdown-preview-mode)
  (autoload 'markdown-mode "markdown-mode"
    "Major mode for editing Markdown files" t)
  (add-to-list 'auto-mode-alist
               '("\\.\\(?:md\\|markdown\\|mkd\\|mdown\\|mkdn\\|mdwn\\)\\'" . markdown-mode))

  (autoload 'gfm-mode "markdown-mode"
    "Major mode for editing GitHub Flavored Markdown files" t)
  (add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
  (require 'markdown-preview-mode)
  (add-to-list 'markdown-preview-stylesheets "https://raw.githubusercontent.com/richleland/pygments-css/master/emacs.css")
  (add-to-list 'markdown-preview-javascript "http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML")
  (add-to-list 'markdown-preview-javascript '("http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML" . async)))

(when +visual-regexp-enabled
  (+install-package 'visual-regexp)
  (require 'visual-regexp)
  (define-key global-map (kbd "C-c R") 'vr/replace)
  (define-key global-map (kbd "C-c q") 'vr/query-replace)
  (when (featurep 'multiple-cursors)
    (define-key global-map (kbd "C-c M-m") 'vr/mc-mark)))

(when +undo-fu-enabled
  (+install-package 'undo-fu)
  (+install-package 'undo-fu-session)
  (require 'undo-fu)
  (require 'undo-fu-session)
  (setq undo-limit 400000
        undo-strong-limit 3000000
        undo-outer-limit 48000000)
  (global-unset-key (kbd "C-z")) ; previously `suspend-frame'
  (unless (featurep' evil)
    (global-set-key (kbd "C-z") 'undo-fu-only-undo)
    (global-set-key (kbd "C-S-z") 'undo-fu-only-redo))
  (setq undo-fu-session-incompatible-files '("\\.gpg$" "/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'"))
  (global-undo-fu-session-mode)
  (with-eval-after-load 'evil
    (customize-set-variable 'evil-undo-system 'undo-fu)))

(when +block-nav-enabled
  (+install-package 'block-nav)
  (setf block-nav-move-skip-shallower t
        block-nav-center-after-scroll t)
  (when (featurep 'hydra)
    (defhydra hydra-block-nav (global-map "C-c b"
                                          :pre (linum-mode 1)
                                          :post (linum-mode -1))
      "block-nav"
      ("M-p" block-nav-previous-indentation-level "previous indent")
      ("M-n" block-nav-next-indentation-level "next indent")
      ("p" block-nav-previous-block "previous block")
      ("n" block-nav-next-block "next block"))))

(when +rust-mode-enabled
  (+install-package 'rust-mode)
  (require 'rust-mode)
  (add-hook 'rust-mode-hook
            (lambda () (prettify-symbols-mode)))
  (add-hook 'rust-mode-hook
            (lambda () (setq indent-tabs-mode nil)))
  (add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode)))

(when +ansible-enabled
  (+install-package 'ansible)
  (+install-package 'ansible-doc)
  (setq +jinja2-mode-enabled t)
  (require 'ansible)
  (add-hook 'yaml-mode-hook #'(lambda () (ansible 1)))
  (add-hook 'ansible-hook 'ansible-auto-decrypt-encrypt)
  (setq ansible-section-face 'font-lock-variable-name-face
        ansible-task-label-face 'font-lock-doc-face)
  ;; set keybinding in yaml-mode since I usually set secrets in variable file
  (define-key yaml-mode-map (kbd "C-c a d") 'ansible-decrypt-buffer)
  (define-key yaml-mode-map (kbd "C-c a e") 'ansible-encrypt-buffer))

(when +jinja2-mode-enabled
  (+install-package 'jinja2-mode)
  (setq jinja2-enable-indent-on-save nil)
  (add-to-list 'auto-mode-alist '("'\\.j2$\\'" . jinja2-mode)))

(when +hydra-enabled
  (+install-package 'hydra)
  (require 'hydra)
  (setq hydra-look-for-remap t))

(when +evil-enabled
  (customize-set-variable 'evil-move-beyond-eol t)
  (customize-set-variable 'evil-split-window-below t)
  (customize-set-variable 'evil-vsplit-window-right t)
  (customize-set-variable 'evil-start-of-line t)
  (customize-set-variable 'evil-want-keybinding nil)
  (customize-set-variable 'evil-want-integration t)
  (customize-set-variable 'evil-want-c-i-jump nil)
  (customize-set-variable 'evil-ex-search-vim-style-regexp t)
  (customize-set-variable 'evil-ex-visual-char-range t)
  (customize-set-variable 'evil-symbol-word-search t)
  (customize-set-variable 'evil-ex-interactive-search-highlight 'selected-window)
  (customize-set-variable 'evil-kbd-macro-suppress-motion-error t)
  (customize-set-variable 'evil-want-Y-yank-to-eol t)
  (customize-set-variable 'evil-want-C-g-bindings t)
  (customize-set-variable 'evil-respect-visual-line-mode nil)
  (customize-set-variable 'evil-want-abbrev-expand-on-insert-exit nil)
  (+install-package 'evil)
  (+install-package 'evil-collection)
  (+install-package 'evil-easymotion)
  (+install-package 'evil-embrace)
  (+install-package 'evil-escape)
  (+install-package 'evil-org)
  (+install-package 'goto-chg)
  (setq evil-ex-search-vim-style-regexp t
        evil-ex-visual-char-range t  ; column range for ex commands
        evil-mode-line-format 'nil
        ;; more vim-like behavior
        evil-symbol-word-search t
        ;; Only do highlighting in selected window so that Emacs has less work
        ;; to do highlighting them all.
        evil-ex-interactive-search-highlight 'selected-window
        ;; It's infuriating that innocuous "beginning of line" or "end of line"
        ;; errors will abort macros, so suppress them:
        evil-kbd-macro-suppress-motion-error t
        ;; also enable `general' by default
        +general-enabled t)
  (add-hook 'with-editor-mode-hook 'evil-insert-state)
  (setq +undo-fu-enabled t)
  (+install-package 'undo-fu)
  (+install-package 'undo-fu-session)
  (require 'undo-fu)
  (require 'evil)
  (require 'goto-chg)
  (evil-mode 1)
  (when (require 'evil-collection nil t)
    (evil-collection-init))
  (with-eval-after-load 'eldoc
    (eldoc-add-command 'evil-normal-state
                       'evil-insert
                       'evil-change
                       'evil-delete
                       'evil-replace))
  ;; (when +multiple-cursors-enabled
  ;;   (require 'evil-mc)
  ;;   (evil-mc-mode 1))
  (require 'evil-easymotion)
  ;;(evilem-default-keybindings)
  (evilem-create 'evilem-default-keybindings)
  (evilem-make-motion evilem-motion-search-next #'evil-ex-search-next
                      :bind ((evil-ex-search-highlight-all nil)))
  (evilem-make-motion evilem-motion-search-previous #'evil-ex-search-previous
                      :bind ((evil-ex-search-highlight-all nil)))
  (evilem-make-motion evilem-motion-search-word-forward #'evil-ex-search-word-forward
                      :bind ((evil-ex-search-highlight-all nil)))
  (evilem-make-motion evilem-motion-search-word-backward #'evil-ex-search-word-backward
                      :bind ((evil-ex-search-highlight-all nil)))
  (put 'visible 'bounds-of-thing-at-point (lambda () (cons (window-start) (window-end))))
  (evilem-make-motion evilem-motion-forward-word-begin #'evil-forward-word-begin :scope 'visible)
  (evilem-make-motion evilem-motion-forward-WORD-begin #'evil-forward-WORD-begin :scope 'visible)
  (evilem-make-motion evilem-motion-forward-word-end #'evil-forward-word-end :scope 'visible)
  (evilem-make-motion evilem-motion-forward-WORD-end #'evil-forward-WORD-end :scope 'visible)
  (evilem-make-motion evilem-motion-backward-word-begin #'evil-backward-word-begin :scope 'visible)
  (evilem-make-motion evilem-motion-backward-WORD-begin #'evil-backward-WORD-begin :scope 'visible)
  (evilem-make-motion evilem-motion-backward-word-end #'evil-backward-word-end :scope 'visible)
  (evilem-make-motion evilem-motion-backward-WORD-end #'evil-backward-WORD-end :scope 'visible)
  (customize-set-variable 'evil-embrace-show-help-p nil)
  (add-hook 'LaTex-mode-hook #'embrace-LaTex-mode-hook)
  (add-hook 'org-mode-hook #'embrace-org-mode-hook)
  (add-hook 'ruby-mode-hook #'embrace-ruby-mode-hook)
  (add-hook 'emacs-lisp-mode-hook #'embrace-emacs-lisp-mode-hook)
  (require 'evil-embrace)
  (with-eval-after-load 'evil-surround
    (evil-embrace-enable-evil-surround-integration))
  (global-evil-surround-mode 1)
  (customize-set-variable 'evil-escape-excluded-states '(normal visual multiedit emacs motion))
  (customize-set-variable 'evil-escape-excluded-major-modes '(treemacs-mode vterm-mode))
  (customize-set-variable 'evil-escape-key-sequence "jk")
  (customize-set-variable 'evil-escape-delay 0.15)
  (evil-define-key* '(insert replace visual operator) 'global "\C-g" #'evil-escape)
  (require 'evil-escape)
  (require 'evil-org)
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'org-capture-mode-hook #'evil-insert-state)

  (evil-org-set-key-theme)
  (setq evil-org-retain-visual-state-on-shift t
        evil-org-special-o/O '(table-row)
        evil-org-use-additional-insert t)

  (add-hook 'evil-org-mode-hook #'evil-normalize-keymaps)

  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys)
  (add-to-list '+diminished-list 'evil-org-mode)
  (add-to-list '+diminished-list 'evil-collection-unimpaired-mode)
  (when (featurep 'treemacs)
    (require 'treemacs-evil)))

(when +general-enabled
  (+install-package 'general)
  (require 'general)
  (require 'config-keybindings))

(when +aggressive-indent-enabled
  (+install-package 'aggressive-indent)
  (require 'aggressive-indent)
  (add-hook 'prog-mode-hook #'aggressive-indent-mode)
  (add-to-list '+diminished-list 'aggressive-indent-mode))

(when +docker-enabled
  (+install-package 'docker)
  (+install-package 'docker-tramp)
  (+install-package 'dockerfile-mode)
  (+install-package 'docker-compose-mode)
  (require 'docker)
  (require 'docker-tramp)
  (require 'dockerfile-mode)
  (require 'docker-compose-mode)
  (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))
  (with-eval-after-load 'tramp
    (add-to-list 'tramp-remote-path 'tramp-own-remote-path)))

(when +org-ref-enabled
  (+install-package 'org-ref)
  (require 'bibtex)
  (require 'org-ref)
  (setq bibtex-completion-bibliography '("~/Dropbox/emacs/bibliography/references.bib"
                                         "~/Dropbox/emacs/bibliography/dei.bib"
                                         "~/Dropbox/emacs/bibliography/master.bib"
                                         "~/Dropbox/emacs/bibliography/archive.bib")
        bibtex-completion-library-path '("~/Dropbox/emacs/bibliography/bibtex-pdfs/")
        bibtex-completion-notes-path "~/Dropbox/emacs/bibliography/notes/"
        bibtex-completion-notes-template-multiple-files "* ${author-or-editor}, ${title}, ${journal}, (${year}) :${=type=}: \n\nSee [[cite:&${=key=}]]\n"

        bibtex-completion-additional-search-fields '(keywords)
        bibtex-completion-display-formats
        '((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
          (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
          (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
          (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
          (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
        bibtex-completion-pdf-open-function
        (lambda (fpath)
          (call-process "open" nil 0 nil fpath)))
  (setq bibtex-autokey-year-length 4
        bibtex-autokey-name-year-separator "-"
        bibtex-autokey-year-title-separator "-"
        bibtex-autokey-titleword-separator "-"
        bibtex-autokey-titlewords 2
        bibtex-autokey-titlewords-stretch 1
        bibtex-autokey-titleword-length 5)
  (define-key bibtex-mode-map (kbd "H-b") 'org-ref-bibtex-hydra/body))

(when +org-roam-enabled
  (+install-package 'org-roam)
  (+install-package 'org-roam-bibtex)
  (+install-package 'emacsql)
  (+install-package 'emacsql-sqlite3)
  (unless (executable-find "guix")
    ;; in guix package the extensions/ folder
    ;; is included in the `emacs-org-roam' package.
    (+install-package 'org-roam-ui))
  (when (featurep 'consult)
    (+install-package 'consult-org-roam))
  (require 'emacsql)
  (require 'emacsql-sqlite)
  (when (string= "berkeley-unix" system-type)
    (setq emacsql-data-root
          (concat "/usr/local/share/emacs/"
                  emacs-version
                  "/site-lisp/emacsql/"))
    (setq emacsql-sqlite-data-root emacsql-data-root)
    (setq emacsql-sqlite-executable "/usr/local/bin/emacsql-sqlite"))
  (add-hook 'org-roam-backlinks-mode 'turn-on-visual-line-mode)
  ;; Make org-roam buffer sticky; i.e. don't replace it when opening a
  ;; file with an *-other-window command.
  (setq org-roam-buffer-window-parameters '((no-delete-other-windows . t)))
  (setq org-roam-link-use-custom-faces 'everywhere)
  (setq org-roam-completion-everywhere t)

  (setq org-roam-directory
        (file-name-as-directory
         (file-truename
          (expand-file-name "roam" org-directory)))
        org-roam-completion-everywhere t
        org-roam-mode-section-functions
        #'(org-roam-backlinks-section
           org-roam-reflinks-section))
  (unless (file-directory-p org-roam-directory)
    (make-directory org-roam-directory :parents))
  (org-roam-db-autosync-mode)

  ;; Hide the mode line in the org-roam buffer, since it serves no purpose. This
  ;; makes it easier to distinguish from other org buffers.
  (add-hook 'org-roam-buffer-prepare-hook #'hide-mode-line-mode)
  (require 'org-roam-dailies)
  (setq org-roam-dailies-directory
        (file-name-as-directory
         (file-truename
          (expand-file-name "daily" org-roam-directory))))
  (require 'org-roam-graph)
  (setq org-roam-node-display-template
        (concat "${title:*} "
                (propertize "${tags:10}" 'face 'org-tag)))

  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-side-window)
                 (side . right)
                 (slot . 0)
                 (window-width . 0.33)
                 (window-parameters . ((no-other-window . t)
                                       (no-delete-other-windows . t)))))
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start nil)
  (require 'server)
  (unless (server-running-p)
    (server-start))
  (require 'org-protocol)
  (require 'org-roam-protocol)
  (setq org-roam-capture-templates
        (quote (("r" "reference" plain "%?"
                 :if-new (file+head "reference/${title}.org" "#+title: ${title}\n")
                 :immediate-finish t
                 :unnarrowed t)
                ("a" "article" plain "%?"
                 :if-new (file+head "articles/${title}.org" "#+title: ${title}\n#+filetags: :article:\n")
                 :immediate-finish t
                 :unnarrowed t)
                ("d" "default" plain
                 "* ${title}\n%?" :if-new
                 (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                            "#+title: ${title}\n#+filetags:\n#+date: %<%Y-%m-%d>\n\n")
                 :unnarrowed t)
                )))
  (setq org-roam-capture-ref-templates
        (quote (("r" "ref" plain
                 "%?"
                 :if-new (file+head
                          "%(format-time-string \"%Y-%m-%d--%H-%M-%SZ--${slug}.org\" (current-time) t)"
                          "#+title: ${title}\n#+filetags:\n#+date: %<%Y-%m-%d>\n\n")
                 :unnarrowed t :jump-to-captured t))))
  (setq org-roam-dailies-capture-templates
        (quote (("d" "Default" plain
                 "%?"
                 :if-new (file+head
                          "%(format-time-string \"%Y-%m-%d--journal.org\" (current-time) t)"
                          "#+title: Journal %<%Y-%m-%d>\n#+date: %<%Y-%m-%d>\n#+journal: private journal\n\n\n")
                 :unnarrowed t))))
  (when (featurep 'consult)
    (require 'consult-org-roam)
    (consult-org-roam-mode 1)
    (add-to-list '+diminished-list 'consult-org-roam-mode)))

(when +elfeed-enabled
  (+install-package 'elfeed)
  (+install-package 'elfeed-org)
  (require 'elfeed)
  (require 'elfeed-org)
  (setq rmh-elfeed-org-files (list (concat org-directory "/elfeed.org")))
  (elfeed-org)
  (defvar +config/elfeed-update-complete-hook nil
    "Functions called with no arguments when `elfeed-update' is finished.")

  (defvar +config/elfeed-updates-in-progress 0
    "Number of feed updates in-progress.")

  (defvar +config/elfeed-search-update-filter nil
    "The filter when `elfeed-update' is called.")

  (defun +config/elfeed-update-complete-hook (&rest ignore)
    "When update queue is empty, run `+config/elfeed-update-complete-hook' functions."
    (when (= 0 +config/elfeed-updates-in-progress)
      (run-hooks '+config/elfeed-update-complete-hook)))

  (add-hook 'elfeed-update-hooks #'+config/elfeed-update-complete-hook)

  (defun +config/elfeed-update-message-completed (&rest _ignore)
    (message "Feeds updated")
    (notifications-notify :title "Elfeed" :body "Feeds updated."))

  (add-hook '+config/elfeed-update-complete-hook #'+config/elfeed-update-message-completed)

  (defun +config/elfeed-search-update-restore-filter (&rest ignore)
    "Restore filter after feeds update."
    (when +config/elfeed-search-update-filter
      (elfeed-search-set-filter +config/elfeed-search-update-filter)
      (setq +config/elfeed-search-update-filter nil)))

  (add-hook '+config/elfeed-update-complete-hook #'+config/elfeed-search-update-restore-filter)

  (defun +config/elfeed-search-update-save-filter (&rest ignore)
    "Save and change the filter while updating."
    (setq +config/elfeed-search-update-filter elfeed-search-filter)
    (setq elfeed-search-filter "#0"))

  ;; NOTE: It would be better if this hook were run before starting the feed updates, but in
  ;; `elfeed-update', it happens afterward.
  (add-hook 'elfeed-update-init-hooks #'+config/elfeed-search-update-save-filter)

  (defun +config/elfeed-update-counter-inc (&rest ignore)
    (cl-incf +config/elfeed-updates-in-progress))

  (advice-add #'elfeed-update-feed :before #'+config/elfeed-update-counter-inc)

  (defun +config/elfeed-update-counter-dec (&rest ignore)
    (cl-decf +config/elfeed-updates-in-progress)
    (when (< +config/elfeed-updates-in-progress 0 ; >
             )
      ;; Just in case
      (setq +config/elfeed-updates-in-progress 0)))

  (add-hook 'elfeed-update-hooks #'+config/elfeed-update-counter-dec))

(when +org-journal-enabled
  (+install-package 'org-journal)
  (setq org-journal-dir (expand-file-name "journal" org-directory)
        org-journal-enable-agenda-integration t
        org-journal-enable-cache t
        org-journal-follow-mode t
        org-journal-enable-encryption t
        org-journal-encrypt-journal t
        org-journal-prefix-key "C-c j")

  (defun +org-journal-find-location ()
    "Open today's journal, but specify a non-nil prefix argument in order to
      inhibit inserting the heading; org-capture will insert the heading."
    (org-journal-new-entry t)
    (unless (eq org-journal-file-type 'daily)
      (org-narrow-to-subtree))
    (goto-char (point-max)))

  (defvar +org-journal--date-location-scheduled-time nil)

  (defun +org-journal-date-location (&optional scheduled-time)
    (let ((scheduled-time (or scheduled-time (org-read-date nil nil nil "Date:"))))
      (setq +org-journal--date-location-scheduled-time scheduled-time)
      (org-journal-new-entry t (org-time-string-to-time scheduled-time))
      (unless (eq org-journal-file-type 'daily)
        (org-narrow-to-subtree))
      (goto-char (point-max))))

  (defun +org-journal-save-entry-and-exit()
    "Simple convenience function.
   Saves the buffer of the current day's entry and kills the window
   Similar to org-capture like behavior"
    (interactive)
    (save-buffer)
    (kill-buffer-and-window))

  (require 'org-journal)
  (require 'org-capture)
  (setq org-capture-templates
        (append org-capture-templates
                '(("j" "Journal")
                  ("jt" "Journal Today" plain (function +org-journal-find-location)
                   "** %(format-time-string org-journal-time-format)%^{Title}\n%i%?"
                   :jump-to-captured t
                   :immediate-finish t)
                  ("js" "Scheduled Journal" plain (function +org-journal-date-location)
                   "** TODO %?\n <%(princ +org-journal--date-location-scheduled-time)>\n"
                   :jump-to-captured t)))))

(when +projectile-enabled
  (+install-package 'projectile)
  (require 'projectile)
  (when (featurep 'consult)
    (+install-package 'consult-projectile)
    (require 'consult-projectile))
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (when (featurep 'treemacs)
    (require 'treemacs-projectile)))

(add-to-list '+diminished-list 'projectile-mode)

(when +lsp-enabled
  (+install-package 'lsp-mode)
  (+install-package 'lsp-ui)
  (when (featurep 'consult)
    (+install-package 'consult-lsp))
  (when (featurep 'treemacs)
    (+install-package 'lsp-treemacs)
    (lsp-treemacs-sync-mode 1))
  (setq lsp-keymap-prefix "C-c L")
  (defun +config/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless))) ;; Configure flex

  (let ((taplo-binary
         (cond ((file-exists-p "/usr/bin/taplo")
                "/usr/bin/taplo")
               ((file-exists-p "/usr/local/bin/taplo")
                "/usr/local/bin/taplo")
               ((file-exists-p (concat (getenv "HOME") ".local/bin/taplo"))
                (concat (getenv "HOME") ".local/bin/taplo")))))
    (when taplo-binary
      (customize-set-variable 'lsp-toml-command taplo-binary)))

  (require 'lsp-mode)
  (add-hook 'c-mode-hook #'lsp-deferred)
  (add-hook 'c++-mode-hook #'lsp-deferred)
  (add-hook 'shell-mode-hook #'lsp-deferred)
  (add-hook 'yaml-mode-hook #'lsp-deferred)
  (add-hook 'rust-mode-hook #'lsp-deferred)
  ;;(add-hook 'toml-mode-hook #'lsp)
  (add-hook 'python-mode-hook #'lsp-deferred)
  (add-hook 'nix-mode-hook #'lsp-deferred)
  (add-hook 'nginx-mode-hook #'lsp-deferred)
  (add-hook 'markdown-mode-hook #'lsp-deferred)
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (add-hook 'lsp-completion-mode-hook #'+config/lsp-mode-setup-completion)

  (when (featurep 'consult)
    (straight-use-package 'consult-lsp)
    (define-key lsp-mode-map [remap xref-find-apropos] #'consult-lsp-symbols)))

(when (executable-find "pass")
  (+install-package 'pass)
  (+install-package 'password-store)
  (+install-package 'password-store-otp)
  (setq password-store-password-length 12)
  (auth-source-pass-enable))

(when +git-modes-enabled
  (+install-package 'git-modes)
  (require 'git-modes)
  (add-to-list 'auto-mode-alist
               (cons "/.dockerignore\\'" 'gitignore-mode)
               (cons "/.griveignore\\'" 'gitignore-mode)))

(when +sly-enabled
  (+install-package 'sly)
  (add-hook 'sly-mode-hook
            (lambda ()
              (unless (sly-connected-p)
                (save-excursion (sly)))))
  (setq sly-contribs '(sly-fancy
                       sly-fontifying-fu
                       sly-profiler
                       sly-retro
                       sly-scratch)))

(when +gist-enabled
  (+install-package 'gist)
  (require 'gist))

(when (executable-find "rg")
  (+install-package 'ripgrep)
  (require 'ripgrep))

(when +emojify-enabled
  (+install-package 'emojify)
  (add-hook 'after-init-hook #'global-emojify-mode))

(when +selectric-enabled
  (+install-package 'selectric-mode)
  (defun +toggle-selectric ()
    "Toggle `selectric-mode'."
    (interactive)
    (if (equal nil selectric-mode)
        (selectric-mode 1)
      (selectric-mode -1)))
  (add-to-list '+diminished-list 'selectric-mode))

(when +symbol-overlay-enabled
  (+install-package 'symbol-overlay)
  (require 'symbol-overlay)
  (require 'transient)
  (transient-define-prefix symbol-overlay-transient ()
    "Symbol Overlay transient"
    ["Symbol Overlay"
     ["Overlays"
      ("." "Add/Remove at point" symbol-overlay-put)
      ("k" "Remove All" symbol-overlay-remove-all)
      ]
     ["Move to Symbol"
      ("n" "Next" symbol-overlay-switch-forward)
      ("p" "Previous" symbol-overlay-switch-backward)
      ]
     ["Other"
      ("m" "Highlight symbol-at-point" symbol-overlay-mode)
      ]
     ]
    )
  (when +evil-enabled
    (evil-define-key
      '(normal visual)
      'global
      "gs" 'symbol-overlay-transient))
  (add-to-list '+diminished-list 'symbol-overlay-mode))

(when +tempel-enabled
  (+install-package 'tempel)
  ;;(customize-set-value 'tempel-trigger-prefix "<")
  (defun tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions)))

  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf))

(+install-package 'restart-emacs)

(when (and +notmuch-enabled
           (executable-find "notmuch"))
  (+install-package 'notmuch)
  (+install-package 'ol-notmuch)
  (+install-package 'gnus-alias)
  (require 'notmuch)
  (require 'ol-notmuch)
  (when +completion-uses-vertico
    (+install-package 'consult-notmuch)
    (require 'consult-vertico))
  (customize-set-variable 'notmuch-init-file (expand-file-name "notmuch-config" +emacs-data-dir))
  (customize-set-variable 'notmuch-search-oldest-first nil)
  (if (executable-find "gpg2")
      (customize-set-variable 'notmuch-crypto-gpg-program "gpg2")
    (customize-set-variable 'notmuch-crypto-gpg-program "gpg"))
  (require 'transient)
  (global-set-key (kbd "<XF86Mail>") 'notmuch)

  ;; functions
  (defun +my/offlineimap-poll (&optional args)
    "Poll offlineimap with ARGS if provided."
    (setq-local output-buffer "*offlineimap*"
                error-buffer "*offlineimap-error*"
                command "offlineimap")
    (if args
        (async-shell-command (mapconcat #'identity `(,command ,@args) " ") output-buffer error-buffer))
    (async-shell-command (mapconcat #'identity `(,command ,@args) " ") output-buffer error-buffer))

  (defun +my/interactive-offlineimap-poll (&optional args)
    "Runs offlineimap interactively.
    Should be run with transient."
    (interactive
     (list (transient-args '+my/offlineimap-transient)))
    (let ((command "offlineimap")
          (output-buffer "*offlineimap*")
          (error-buffer "*offlineimap-error*"))
      (if args
          (async-shell-command (mapconcat #'identity `(,command ,@args) " ") output-buffer error-buffer)
        (async-shell-command (mapconcat #'identity `(,command ,@args) " ") output-buffer error-buffer))))

  (transient-define-prefix +my/offlineimap-transient ()
    "Offlineimap transient account chooser"
    :man-page "offlineimap"
    :incompatible '(("-a gmail" "-a yahoo" "-a ymail" "-a hotmail"))
    ["Account"
     ("g" "Gmail" "-a gmail")
     ("y" "Yahoo" "-a yahoo")
     ("Y" "Ymail" "-a ymail")
     ("h" "Hotmail" "-a hotmail")]
    ["Options"
     ("-d" "dry-run" "--dry-run")
     ("-i" "info" "--info")
     ("-1" "single-thread" "-1")
     ("-s" "syslog" "-s")
     ("-q" "quick" "-q")]
    ["Actions"
     ("p" "poll offlineimap" +my/interactive-offlineimap-poll)])

  (when (executable-find "offlineimap")
    (define-key notmuch-common-keymap [remap notmuch-poll-and-refresh-this-buffer] #'+my/offlineimap-transient))

  (require 'gnus-alias)
  (add-hook 'message-setup-hook
            (lambda ()
              (gnus-alias-determine-identity)
              (define-key message-mode-map (kbd "C-c f")
                (lambda ()
                  (interactive)
                  (message-remove-header "Fcc")
                  (message-remove-header "Organization")
                  (gnus-alias-select-identity)
                  (when (featurep 'notmuch)
                    (notmuch-fcc-header-setup))))))

  ;; tagging-keys
  ;; list of three elements
  (customize-set-variable
   'notmuch-tagging-keys
   '(("a" notmuch-archive-tags "Archive")
     ("u" notmuch-show-mark-read-tags "Mark read")
     ("f" ("+flagged") "Flag")
     ("s" ("+spam" "-inbox") "Mark as spam")
     ("d" ("+deleted" "-inbox") "Delete")))

  ;; saved-searches
  (customize-set-variable 'notmuch-saved-searches
                          '((:name "inbox" :query "tag:inbox" :key "i")
                            (:name "unread" :query "tag:unread" :key "u")
                            (:name "flagged" :query "tag:flagged" :key "f") ;starred in gmail
                            (:name "sent" :query "tag:sent" :key "t")
                            (:name "drafts" :query "tag:draft" :key "d")
                            (:name "all mail" :query "*" :key "a")
                            (:name "Today"
                                   :query "date:today AND NOT tag:spam AND NOT tag:bulk"
                                   :key "T"
                                   :search-type 'tree
                                   :sort-order 'newest-first)
                            (:name "This Week"
                                   :query "date:weeks AND NOT tag:spam AND NOT tag:bulk"
                                   :key "W"
                                   :search-type 'tree
                                   :sort-order 'newest-first)
                            (:name "This Month"
                                   :query "date:months AND NOT tag:spam AND NOT tag:bulk"
                                   :key "M"
                                   :search-type 'tree
                                   :sort-order 'newest-first)
                            (:name "flagged"
                                   :query "tag:flagged AND NOT tag:spam AND NOT tag:bulk"
                                   :key "f"
                                   :search-type 'tree
                                   :sort-order 'newest-first)
                            (:name "spam" :query "tag:spam")
                            (:name "gmail/inbox" :query "tag:gmail/inbox")
                            (:name "gmail/sent" :query "tag:gmail/sent")
                            (:name "gmail/draft" :query "tag:gmail/draft")
                            (:name "gmail/archive" :query "tag:gmail/archive")
                            (:name "gmail/spam" :query "tag:gmail/spam")
                            (:name "yahoo/inbox" :query "tag:yahoo/inbox")
                            (:name "yahoo/sent" :query "tag:yahoo/sent")
                            (:name "yahoo/draft" :query "tag:yahoo/draft")
                            (:name "yahoo/archive" :query "tag:yahoo/archive")
                            (:name "yahoo/spam" :query "tag:yahoo/spam")
                            (:name "hotmail/inbox" :query "tag:hotmail/inbox")
                            (:name "hotmail/sent" :query "tag:hotmail/sent")
                            (:name "hotmail/draft" :query "tag:hotmail/draft")
                            (:name "hotmail/archive" :query "tag:hotmail/archive")
                            (:name "hotmail/spam" :query "tag:hotmail/spam")
                            (:name "ymail/inbox" :query "tag:ymail/inbox")
                            (:name "ymail/sent" :query "tag:ymail/sent")
                            (:name "ymail/draft" :query "tag:ymail/draft")
                            (:name "ymail/archive" :query "tag:ymail/archive")
                            (:name "ymail/spam" :query "tag:ymail/spam")))
  )

(provide 'config-packages)
;;; config-packages.el ends here

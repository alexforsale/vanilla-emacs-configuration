;;; config-nixos.el --- distro variables and helper functions -*- lexical-binding: t -*-
;;; Commentary:
;;
;;; Code:

(setq +nix-enabled t)

(provide 'config-nixos)
;;; config-nixos.el ends here

;;; config-guix.el --- guix init -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(when (executable-find "guile")
  (+install-package 'geiser)
  (+install-package 'geiser-guile)
  (require 'geiser)
  (require 'geiser-guile)
  (let ((guile-bin (executable-find "guile")))
    (customize-set-variable 'geiser-guile-binary (list guile-bin "--no-auto-compile"))))

(when (not (directory-empty-p "/run/current-system/profile/share/guile/site/3.0"))
  (add-to-list 'geiser-guile-load-path "/run/current-system/profile/share/guile/site/3.0"))
(when (not (directory-empty-p
            (concat (getenv "HOME") "/.guix-profile/share/guile/site/3.0")))
  (add-to-list 'geiser-guile-load-path
               (concat (getenv "HOME")"/.guix-profile/share/guile/site/3.0")))
(when (not (directory-empty-p
            (concat (getenv "HOME") "/.guix-home/profile/share/guile/site/3.0")))
  (add-to-list 'geiser-guile-load-path
               (concat (getenv "HOME")"/.guix-home/profile/share/guile/site/3.0")))

(provide 'config-guix)
;;; config-guix.el ends here

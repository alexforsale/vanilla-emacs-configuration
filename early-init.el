;;; early-init.el --- Customization before normal init -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(add-to-list 'load-path (expand-file-name "modules/" user-emacs-directory))

(require 'environment)

(unless (string-match "/usr/bin" (getenv "PATH"))
  (setenv "PATH" (concat (getenv "PATH") ":/usr/bin")))

(unless (string-match (concat (getenv "HOME") "/.local/bin") (getenv "PATH"))
  (setenv "PATH" (concat (getenv "PATH") ":" (getenv "HOME")":/.local/bin")))

;; prefer `straight.el' when there's git available
(when (executable-find "git")
  (setq package-enable-at-startup nil))

(when +minimal-ui
  (progn
    (setq inhibit-startup-message t)
    (push '(tool-bar-lines . 0) default-frame-alist)
    (push '(menu-bar-lines . 0) default-frame-alist)
    (push '(vertical-scroll-bars) default-frame-alist)
    (push '(background-color . "#232635") default-frame-alist)
    (push '(foreground-color . "#FCFCFA") default-frame-alist)
    (push '(mouse-color . "white") default-frame-alist)))

(provide 'early-init)
;;; early-init.el ends here

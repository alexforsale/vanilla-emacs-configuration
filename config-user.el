;;; config-user.el --- Emacs configuration file -*- lexical-binding: t mode: emacs-lisp -*-
;;; Commentary:
;;; Code:

(setq user-full-name "Kristian Alexander P"
      user-mail-address "alexforsale@yahoo.com")
(customize-set-variable 'org-directory (expand-file-name "Documents/google-drive/org/" (getenv "HOME")))
(customize-set-variable '+org-archives-file (expand-file-name "archives.org" org-directory))
(customize-set-variable '+org-capture-todo-file (expand-file-name "todo.org" org-directory))
(customize-set-variable '+org-capture-links-file (expand-file-name "links.org" org-directory))
(customize-set-variable '+org-capture-notes-file (expand-file-name "notes.org" org-directory))
(customize-set-variable '+org-capture-habits-file (expand-file-name "habits.org" org-directory))
(customize-set-variable '+org-capture-projects-file (expand-file-name "projects.org" org-directory))
(customize-set-variable 'org-agenda-files (list org-directory))
(customize-set-variable '+emms-music-dir "~/Music")
(if (version< emacs-version "28")
    (customize-set-variable '+config-standard-theme 'wombat)
  (customize-set-variable '+config-standard-theme 'modus-vivendi))
(cond ((string= "morocco" system-name)
       (customize-set-variable '+config-doom-theme 'doom-solarized-dark-high-contrast))
      ((string= "burundi" system-name)
       (customize-set-variable '+config-doom-theme 'doom-horizon))
      ((string= "senegal" system-name)
       (customize-set-variable '+config-doom-theme 'doom-gruvbox))
      (t
       (customize-set-variable '+config-doom-theme 'doom-one)))

(customize-set-variable '+mail-directory "~/.mail")

;; example theme customization
(when (string= "doom-solarized-dark"
               (or +config-doom-theme
                   +config-standard-theme))
  (setq doom-solarized-dark-brighter-text t)
  (setq doom-solarized-dark-brighter-comments t)
  (setq doom-solarized-dark-brighter-modeline t))

(with-eval-after-load 'modus-themes
  (setq modus-themes-diffs 'modus-themes-success-deuteranopia
        modus-themes-links '(no-underline faint)
        modus-themes-syntax '(faint alt-syntax)
        modus-themes-region '(bg-only)
        modus-themes-fringes 'subtle
        modus-themes-hl-line '(accented)
        modus-themes-mode-line '(borderless accented)
        modus-themes-prompts '(intense)
        modus-themes-org-blocks 'subtle
        modus-themes-completions 'opinionated
        modus-themes-paren-match 'intense))

(setq +minimal-ui t
      +tab-bar-enabled t
      +savehist-enabled t
      +saveplace-enabled t
      +completion-uses-vertico t
      +flycheck-enabled t
      +vterm-enabled t
      +rainbow-enabled t
      +hl-todo-enabled t
      +diff-hl-enabled t
      +treemacs-enabled t
      +multiple-cursors-enabled t
      +yasnippet-enabled nil
      +yaml-enabled t
      +toml-mode-enabled t
      +guix-enabled t
      +geiser-enabled t
      +nix-mode-enabled t
      +nginx-mode-enabled t
      +markdown-enabled t
      +visual-regexp-enabled t
      +undo-fu-enabled t
      +block-nav-enabled nil
      +vertico-posframe-enabled nil
      +rust-mode-enabled t
      +ansible-enabled t
      +jinja2-mode-enabled t
      +hydra-enabled nil
      +general-enabled t
      +evil-enabled t
      +aggressive-indent-enabled t
      +docker-enabled t
      +org-ref-enabled t
      +org-roam-enabled t
      +elfeed-enabled t
      +org-journal-enabled t
      +projectile-enabled t
      +lsp-enabled t
      +git-modes-enabled t
      +sly-enabled t
      +gist-enabled nil
      +perspective-enabled t
      +emojify-enabled t
      +selectric-enabled t
      +symbol-overlay-enabled t
      +tempel-enabled t
      +notmuch-enabled t)

(provide 'config-user)
;;; config-user.el ends here

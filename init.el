;;; init.el --- init -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(when (executable-find "git")
  (defvar bootstrap-version)
  (let ((bootstrap-file
   (expand-file-name "straight/repos/straight.el/bootstrap.el" +emacs-data-dir))
  (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
  (goto-char (point-max))
  (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(require 'sane-defaults)
(require 'config-packages)

;; per distro configuration
(let ((distro-config (concat "config-" +distro)))
  (when (locate-library distro-config)
    (require (intern distro-config))))

;; diminish at the end
(when (featurep 'diminish)
  (dolist (mode +diminished-list)
    (diminish mode)))

(provide 'init)
;;; init.el ends here
